"use client"
import Styles from '../styles/Testimonials.module.css'
import Heading from './Heading'
import SwiperCore, { Navigation,Pagination,Autoplay } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/swiper-bundle.min.css';
import 'swiper/components/navigation/navigation.min.css'
import 'swiper/components/pagination/pagination.min.css'
import TestimonialsCard from './TestimonialsCard';
SwiperCore.use([Pagination,Autoplay,Navigation ]);
const Testimonials = () => {
  return (
    <section className="section padding-top">
        <div className="container">
        <div className={Styles.testimonialSlider}>
            <div className='layout-grid section-column-grid padding'>
            <div><Heading title={'Testimonials'} align="left" /></div>
            </div>
            <Swiper 
spaceBetween={10}
pagination={{ clickable: true, el:'.swiper-pagination' }}
loop={true}
 autoplay={{
 delay: 2000,
 disableOnInteraction: true
  }}
  breakpoints={{
    // when window width is >= 640px
    768: {
      width: 768,
      slidesPerView: 1,
    },
    768: {
      width: 768,
      slidesPerView: 1,
    },
    1024: {
      width: 1024,
      slidesPerView: 1,
    },
    1240: {
      width: 1200,
      slidesPerView: 1,
    },
    
  }}

>
<SwiperSlide><TestimonialsCard /></SwiperSlide>
<SwiperSlide><TestimonialsCard /></SwiperSlide>
<SwiperSlide><TestimonialsCard /></SwiperSlide>

</Swiper>
<div className="swiper-pagination"></div>
        </div>
        </div>
        </section>
  )
}

export default Testimonials