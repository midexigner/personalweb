import Styles from '../styles/PageHeader.module.css'
import Link from 'next/link'

const PageHeader = ({title,url}) => {
  return (
    <section className={`section ${Styles.pageHeader}`}>
        <div className='container'>
        <h1 className={Styles.headline}>{title} <br/></h1>
        <div className={Styles.breadcrumbsCard}>
            <Link href="/" className={Styles.linkbreadcrumbsCard}>Home</Link>
            <Link href={url} aria-current="page" className={`${Styles.linkbreadcrumbsCard} ${Styles.lightBreadcrumbs} ${Styles.activeBreadcrumbs}`}>{title} <br/></Link></div>
        </div>
    </section>
  )
}

export default PageHeader