import Styles from '../styles/Services.module.css'
import Heading from './Heading'
const Service = () => {
  return (
    <section className='section'>
        <div className='container'>
            <Heading title="Elevate Your Business with Our Comprehensive Digital Marketing Services in Pakistan" />
        <div className='layout-grid features-wrapper'>
            <article className={`${Styles.servicesCard} ${Styles.servicesCardLine}`}>
            {/* <div className={`${Styles.featuresIcon} ${Styles.IconOne}`}></div> */}
            <h5>Custom Websites</h5>
            <p>Operational as a Web Design & Development Company in Pakistan, we create websites that are easier to navigate and give better user experience to the viewers</p>
            </article>
            <article className={`${Styles.servicesCard} ${Styles.servicesCardLine}`}>
            {/* <div className={`${Styles.featuresIcon} ${Styles.IconTwo}`}></div> */}
            <h5>Templated Websites</h5>
            <p>When you need to increase your sales, leads, and conversions instantly, our PPC services can be the answer. We shall run a result-oriented PPC campaign under the direct supervision of our experts.</p>
            </article>
            <article className={`${Styles.servicesCard} ${Styles.servicesCardLine}`}>
            {/* <div className={`${Styles.featuresIcon} ${Styles.IconThree}`}></div> */}
            <h5>Search Engine Optimization</h5>
            <p>By utilizing the white hat practices, using the best SEO tools, and performing in-depth keyword research, we can help steal initial positions for your website in SERPs</p>
            </article>
            <article className={`${Styles.servicesCard} ${Styles.servicesCardLine}`}>
            {/* <div className={`${Styles.featuresIcon} ${Styles.IconThree}`}></div> */}
            <h5>Google Ads Service</h5>
            <p>MI Dexigner tech-enabled PPC management services will assist you in achieving your objectives.
and measuring and maximizing your ROI from PPC, whether your goal is to boost conversions, website traffic, or both.</p>
            </article>
            <article className={`${Styles.servicesCard} ${Styles.servicesCardLine}`}>
            {/* <div className={`${Styles.featuresIcon} ${Styles.IconThree}`}></div> */}
            <h5>Content Marketing Service</h5>
            <p>Elevate your brand's online presence with the comprehensive Content Management Services of MI Dexigner Company.
Our team of experts will craft compelling and engaging content that will help you establish a strong online presence and connect with your target audience.</p>
            </article>
            <article className={`${Styles.servicesCard} ${Styles.servicesCardLine}`}>
            {/* <div className={`${Styles.featuresIcon} ${Styles.IconThree}`}></div> */}
            <h5>SEO Reseller Program</h5>
            <p>Unlock the full potential of your business with the flexible and efficient SEO Reseller Program at MI Dexigner Company. As a provider of outsourced SEO services, we take on the production and management of SEO projects for other companies, allowing them to focus on building their business.
              </p>
            </article>
            
        </div>
        </div>
    </section>
  )
}

export default Service