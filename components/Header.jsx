"use client"
import Link from 'next/link'
import { useState } from 'react';
import Styles from '../styles/Header.module.css'
import Button from './Button'

const Header = () => {
  const [scroll, setScroll] = useState(0);

  const changeColor = () => {
    if (typeof window !== "undefined") {
      if (window.scrollY >= 30) {
        setScroll(true);
      } else {
        setScroll(false);
      }
    }
  };

  if (typeof window !== "undefined") {
    window.addEventListener("scroll", changeColor);
  }

  return (
    // 
    <header className={Styles.stickyHeader}>
    <div className={`${Styles.navbar} ${scroll && Styles.isActive}`}>
    <div className='container navbar-container'>
        <div className={Styles.navColumn}>
            <Link href={'/'} className={Styles.brand}><p className={Styles.logo}>MI Dexigner<span className={Styles.dot}>.</span></p></Link>
        </div>
        {/* <input type='checkbox' id='navbarToggle' className={Styles.navbarToggleInput}/> */}
        <nav className={Styles.navMenu}>
            <ul>
                <li><Link href={'/'}>Home</Link></li>
                <li><Link href={'/services'}>Services</Link></li>
                <li><Link href={'/about-us'}>About Us</Link></li>
                <li><Link href={'/'}>Pricing</Link></li>
                <li><Link href={'/portfolio'}>Our Work</Link></li>
                {/* <li><Link href={'/'}>Blog</Link></li> */}
                <li><Link href={'/contact-us'}>Contact</Link></li>
            </ul>
        </nav>
        <div className={`${Styles.navColumn} ${Styles.right}`}>
          <Button url={'/get-a-quote'} text={'Get A Quote'} />
         <div className={Styles.navbarToggle}>
         <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
  <path strokeLinecap="round" strokeLinejoin="round" d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5" />
</svg>

         </div>
          </div>
    </div>
    </div>
    </header>
  )
}

export default Header