import Head from 'next/head'
import { urlFor } from '../lib/sanity'

const Meta = ({ title,tagline, keywords, description,robots,url,image,siteName }) => {
  
  return (
    <Head>
    <meta charSet="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
      <link rel='icon' href='/favicon.png' />
      <title>{title} {tagline && "| " + tagline}</title>
      <meta name="google-site-verification" content="Y8Huf0eRo_hZ2yIRKYcUJdAMl-U47M6E0ba_veHmtGU" />
      <meta name='keywords' content={keywords} />
      <meta name='description' content={description} />
      <meta name="robots" content={robots} />
      <meta name="googlebot" content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1" />
      <meta name="bingbot" content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1" />
      <link rel="canonical" href={url} />
      <meta property="og:locale" content="en_US" />
      <meta property="og:type" content="website" />
      <meta property="og:title" content={title} />
      <meta property="og:description" content={description} />
      <meta property="og:image" content={image} />
      <meta property="og:url" content={url} />
      <meta property="og:site_name" content={siteName} />
      <meta name="twitter:card" content="summary_large_image" />
      <meta name="twitter:title" content={title} />
      <meta name="twitter:description" content={description} />
      <meta name="twitter:image" content={image} />
      {/* <link rel="preconnect" href="https://fonts.googleapis.com" />
<link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin="true" />
<link href="https://fonts.googleapis.com/css2?family=PT+Sans:wght@400;700&family=Space+Grotesk:wght@300;400;500;600;700&display=swap" rel="stylesheet" /> */}


    </Head>
  )
}

Meta.defaultProps = {
  title: 'Professional Web Developer By MI Dexigner',
  tagline: 'Full Stack Developer',
  keywords: 'website design services, web development services, wordpress page builder, custom web development services, professional web development services, website design & development services',
  description: 'I’m a full-stack developer, trainer, and base in Karachi from Pakistan and working as a Senior staff for Ocean Software Pvt Ltd',
  url: 'https://midexigner.com',
  robots:'index, follow',
  image:'https://scontent.fkhi2-2.fna.fbcdn.net/v/t39.30808-6/292692466_414060420745987_946441481661781475_n.jpg?_nc_cat=103&ccb=1-7&_nc_sid=09cbfe&_nc_ohc=Dktsyt5k4r8AX_dy1Av&_nc_ht=scontent.fkhi2-2.fna&oh=00_AT8b43XOI5E2mwzjDTxXrD2CcI7NtAJH321AzfkcviSJ5w&oe=6347E33B',
  siteName:'MI Dexigner'
}

export default Meta