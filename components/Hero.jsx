import Styles from '../styles/Hero.module.css';
import Button from './Button';
import QueryForm from './QueryForm';

const Hero = () => {
  return (
    <section className={Styles.hero}>
      <div className="container">
        <div className="layout-grid grid-scale asimetric right-side ">
          <div className={Styles.sideTextInfo}>
            <div className={Styles.headerSection}>
              <div className={Styles.heroHeading}>
              Digital marketing agency</div>
              <div className={Styles.textWrapper}>
                <h1 className={Styles.textLine}>Specializing in SEO services.</h1>
              </div>
              <div className="divider" />
              <p>
              In the midst of a digital battlefield among countless online businesses vying for SERP dominance, MI Dexigner digital marketing agency offers its globally competitive SEO and social media marketing services. Empower your business to thrive in the market and excel in profitability with our strategic solutions at your fingertips.</p>
              <div className={Styles.buttonWrapper}>
                <Button url={'/'} text={'Get A Free Consultant'} type="button-color" />
                <Button url={'tel:+923242340583'} text={'Book a Call'} />
              </div>
            </div>
          </div>
          <div className="hero-side-wrapper center">
            <div className={Styles.heroSideImage}>
              <div className={`${Styles.pesonalInfo} ${Styles.heroIn}`}>
                <QueryForm />
              </div>
            </div>
          </div>

        </div>
      </div>
      <div className="testimonial-block-color light right" />
    </section>
  );
};

export default Hero;
