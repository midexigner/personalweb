import React from 'react';
import Styles from '../styles/Video.module.css';
const Video = () => {
  return (
    <section className={Styles.videoSection}>
      <div className={Styles.backgroundVideo}>
        <video
          autoPlay={true}
          loop={true}
          muted={true}
          playsInline={true}
          data-object-fit="cover"
          style={{backgroundImage: `url('/video-poster-00001.jpg')`}}
        >
          <source src="/video-transcode.mp4" />
          <source src="/video-transcode.webm" />

        </video>
        <div className={Styles.overlayVideo}>
          <div className="container center">
            <div className={Styles.sloganWrapper}>
              <div className={Styles.sloganText}>
                <div className={Styles.topTitle}>
                Join our mailing list for updates.<br />
                </div>
                <h2 className="left">
                  The MI Dexigner is the most n
                  <strong className="bold-text">
                    atural &amp; an intuitive 
                  </strong>
                </h2>
                <div className={Styles.formBlock}>
                <form id="email-form" name="email-form" method="post" className='form'>
                <input type="email" className={Styles.inputField} maxLength="256" name="Email" data-name="Email" placeholder="Your email address" id="Email" required />
                <input type="submit" value="Subscribe now"  className={Styles.button}></input>
                </form>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    </section>
  );
};

export default Video;
