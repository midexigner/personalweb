import Styles from '../styles/Testimonials.module.css'

const TestimonialsCard = () => {
  return (
    <div className={Styles.testimonialsCard}>
        <div className={Styles.ctaInfoWrapper}>
          <div className={Styles.testimonialsImage}></div>
          <div className={Styles.ctaInfo}>
          <div className={Styles.testimonialTitle}><strong>MArio Fernandes</strong> - CEO of Calipso</div>
          <div className={Styles.testimonialsContent}>“One day you will wake up and there won’t be any more time to do the things you’ve always wanted. Do it now.”</div>
          </div>
          </div> 
    </div>
  )
}

export default TestimonialsCard