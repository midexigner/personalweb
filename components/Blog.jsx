import Styles from '../styles/Blog.module.css'
import Link from 'next/link'
import Heading from './Heading'

const Blog = () => {
  return (
    <section className='section'>
        <div className='container'>
        <Heading title={'Blog'} buttonURL="/" buttonURLText="View all posts" align="left" />
<div className={Styles.collectionListBlog}>
<div className={Styles.wDynItem}>
<Link href='/' className={Styles.blogCard}>
    <div className={Styles.blogImage}  style={{backgroundImage:`url('/blog-01.jpg')`}}>
        <div className={Styles.imageDate}>
        <div className={Styles.blogDate}>14</div>
        <div className={Styles.bottomDate}><div>May</div></div>
        </div>
    </div>
<div className={Styles.blogContent}>
    <div className={Styles.bottomBlogTags}>
    <div className={Styles.blogTagsText}>Web design</div>
    <div className={Styles.blogTagsText}>Web design</div>
    </div>
<h5 className={Styles.blogTitle}>Change your mind, change your future.</h5>
</div>
</Link>

    </div>
<div className={Styles.wDynItem}>
<Link href='/' className={Styles.blogCard}>
    <div className={Styles.blogImage}  style={{backgroundImage:`url('/blog-01.jpg')`}}>
        <div className={Styles.imageDate}>
        <div className={Styles.blogDate}>14</div>
        <div className={Styles.bottomDate}><div>May</div></div>
        </div>
    </div>
<div className={Styles.blogContent}>
    <div className={Styles.bottomBlogTags}>
    <div className={Styles.blogTagsText}>Web design</div>
    <div className={Styles.blogTagsText}>Web design</div>
    </div>
<h5 className={Styles.blogTitle}>Change your mind, change your future.</h5>
</div>
</Link>

    </div>
</div>
        </div>
    </section>
  )
}

export default Blog