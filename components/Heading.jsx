import Styles from '../styles/Heading.module.css'
import Link from 'next/link'
const Heading = ({title,align,buttonURL,buttonURLText}) => {
  return (
    
    <div className={`${Styles.sectionHeading} ${(align ==="left") ? Styles.left : ""}`}>
      {buttonURLText ?(
        <div className='layout-grid section-column-grid'>
        <div><h2 className={Styles.title}>{title}</h2><div className="divider" /></div>
        <Link href={buttonURL} className={Styles.rightLinkSection}>{buttonURLText}</Link>
        </div>
      ):(
        <>
        <h2 className={Styles.title}>{title}</h2>
        <div className="divider" />
        </>
        )
        }
    </div>
  )
}

export default Heading