"use client";

import { usePathname } from "next/navigation";
import Styles from "../styles/QueryForm.module.css";
import { useEffect, useState } from "react";

const QueryForm = () => {
  const pathname = usePathname();

  const key = '5XpThOAEkfgOvEJ';
  const [formData, setFormData] = useState({
    full_name: '',
    email_address: '',
    phone_number: '',
    project_type: '',
    message: '',
    visitor_ip: '',
    visitor_country: '',
    visitor_city: '',
    visitor_state: '',
  });
  useEffect(() => {
  const fetchData = async () => {
    try {
      const response = await fetch(`//pro.ip-api.com/json/?key=${key}`);
      const data = await response.json();
      if (data) {
        console.log(data);
        setFormData({
          ...formData,
          visitor_ip: data.query || '',
          visitor_country: data.country || '',
          visitor_city: data.city || '',
          visitor_state: data.regionName || '',
        });
      }
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

  fetchData();
}, []);

const handleChange = (e) => {
  const { name, value } = e.target;
  // Check if the input is for the phone_number field
  if (name === 'phone_number') {
    // Remove any non-numeric characters from the input
    const numericValue = value.replace(/\D/g, '');

    // Ensure that the value has at most 10 digits
    const formattedPhoneNumber = numericValue.slice(0, 11);

    // Update the state with the formatted phone number
    setFormData({ ...formData, [name]: formattedPhoneNumber });
  } else {
    // For other fields, update the state as usual
    setFormData({ ...formData, [name]: value });
  }
};

const handleSubmit = (e) => {
   e.preventDefault();
   console.log(formData);
};
  return (
    <div className={Styles.wForm}>
      <form className={Styles.Form} onSubmit={handleSubmit}>
        <div className={`layout-grid ${Styles.gridContact}`}>
          <div>
            <label htmlFor="full_name" className={Styles.contactUsField}>
              Full Name:
            </label>
            <input
              type="text"
              placeholder="Full Name"
              className={`${Styles.wInput} ${Styles.textField}`}
              name="full_name"
              value={formData.full_name} onChange={handleChange}
            />
          </div>
          <div>
            <label htmlFor="email_address" className={Styles.contactUsField}>
              Email Address:
            </label>
            <input
              type="email"
              placeholder="Email Address"
              className={`${Styles.wInput} ${Styles.textField}`}
              name="email_address"
              value={formData.email_address} onChange={handleChange}
            />
          </div>
          <div>
            <label htmlFor="phone_number" className={Styles.contactUsField}>
              Phone Number:
            </label>
            <input
              type="tel"
              placeholder="Phone Number"
              className={`${Styles.wInput} ${Styles.textField}`}
              name="phone_number"
              value={formData.phone_number} onChange={handleChange}
            />
          </div>
          <div>
            <label htmlFor="project_type" className={Styles.contactUsField} name="project_type">
              Project Type:
            </label>
            <select className={`${Styles.wInput} ${Styles.selectField}`}>
              <option value={""}>Please Select</option>
              <option value={"Web Design &amp; Development"}>Web Design &amp; Development</option>
              <option value={"Mobile Apps"}>Mobile Apps</option>
              <option value={"Domain, Hosting &amp; Emails"}>Domain, Hosting &amp; Emails</option>
              <option value={"SEO / SEM / SMM"}>SEO / SEM / SMM</option>
            </select>
          </div>
          <div className={Styles.textAreaSec}>
            <label htmlFor="message" className={Styles.contactUsField}>
              Message:
            </label>
            <textarea
              id="Contact-Message"
              name="message"
              maxLength="5000"
              placeholder="Describe your project..."
              required=""
              className={`${Styles.wInput} ${Styles.textField} ${Styles.message}`}
              value={formData.message} onChange={handleChange}
            ></textarea>
          </div>
        </div>
        <button type="submit" className={`${Styles.button} button-color`}>
          Submit Now
        </button>
        <input type="hidden" name="page_url" value={pathname} />
        <input type="hidden" name="visitor_ip" value={formData.visitor_ip} />
        <input type="hidden" name="visitor_country" value={formData.visitor_country} />
        <input type="hidden" name="visitor_city" value={formData.visitor_city} />
        <input type="hidden" name="visitor_state" value={formData.visitor_state} />
      </form>
    </div>
  );
};

export default QueryForm;
