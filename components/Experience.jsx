import Styles from '../styles/Experience.module.css'
import Heading from './Heading'

const Experience = () => {
  return (
    <section className='pt-5 pb-5'>
        <div className='container'>
        <Heading title={"Why is MI Dexigner So Reliable?"} />
       <div className={Styles.process}>
           <div className={Styles.processLine}></div>
          {/* being */}
          <div className={`${Styles.processCard} ${Styles.processCardLeft} ${Styles.lessMargin}`}>
           <div className={Styles.iconWrapper}>
           <div className={`${Styles.processIcon} ${Styles.processIconCenter}`}></div>
           </div>
           </div>
          {/* end */}

          
          
           
          {/* being 1 */}
          <div className={`${Styles.processCard} ${Styles.processCardLeft}`}>
              <div className={`${Styles.processInfo} ${Styles.processLeft}`}>
              <h3 className={`${Styles.nameExperience} ${Styles.process} ${Styles.experienceRight}`}>Benefitting You from Any Corner of the Globe</h3>
              <p>Achieve global-caliber digital marketing services at highly affordable rates. Engage with MI Dexigner online marketing agency to secure the optimal solution for your business needs.</p>
              </div>
           <div className={Styles.iconWrapper}>
           <div className={`${Styles.processIcon} ${Styles.processIconCenter}`}></div>
           <div className={Styles.horizontalLine}></div>
           </div>
           </div>
          {/* end 1 */}
          {/* being 2 */}
          <div className={`${Styles.processCard} ${Styles.processCardRight}`}>
               <div className={Styles.iconWrapper}>
           <div className={`${Styles.processIcon} ${Styles.processIconCenter}`}></div>
           <div className={Styles.horizontalLine}></div>
           </div>
              <div className={`${Styles.processInfo} ${Styles.processRight}`}>
              <h3 className={`${Styles.nameExperience} ${Styles.process} ${Styles.experienceRight}`}>Perfect Combo of Paid + Organic Digital Marketing Services</h3>
              <p>Our content marketing SEO services include audits and analysis to identify areas where organic reach is achievable and where financial investment is necessary for cost-effective strategies.</p>
              </div>
           </div>
          {/* end 2 */}
            {/* being 3 */}
            <div className={`${Styles.processCard} ${Styles.processCardLeft}`}>
              <div className={`${Styles.processInfo} ${Styles.processLeft}`}>
              <h3 className={`${Styles.nameExperience} ${Styles.process} ${Styles.experienceRight}`}>Visibility Regardless of Business Size</h3>
              <p>Stand toe-to-toe with industry giants with our powerful content marketing agency services. We enhance your brand's online presence, attract potential customers to generate leads, and facilitate rapid conversions.</p>
              </div>
           <div className={Styles.iconWrapper}>
           <div className={`${Styles.processIcon} ${Styles.processIconCenter}`}></div>
           <div className={Styles.horizontalLine}></div>
           </div>
           </div>
          {/* end 3 */}
          
           {/* being 4 */}
<div className={`${Styles.processCard} ${Styles.processCardRight}`}>
               <div className={Styles.iconWrapper}>
           <div className={`${Styles.processIcon} ${Styles.processIconCenter}`}></div>
           <div className={Styles.horizontalLine}></div>
           </div>
              <div className={`${Styles.processInfo} ${Styles.processRight}`}>
              <h3 className={`${Styles.nameExperience} ${Styles.process} ${Styles.experienceRight}`}>Social Media Consulting Services for Unprecedented Growth</h3>
              <p>Our social media consultancy services are backed by solid insights on data, deep observation, proactive vision, and business farsightedness. Our digital marketing consultants facilitate solutions that are tailored to your business expansion prospects.</p>
              </div>
          
           </div>
          {/* end 4 */}
          {/* being bottom line */}
          <div className={`${Styles.processCard} ${Styles.processCardLeft} ${Styles.lessMargin}`}>
           <div className={Styles.iconWrapper}>
           <div className={`${Styles.processIcon} ${Styles.processIconCenter}`}></div>
           </div>
           </div>
          {/* end bottom line */}
       </div>
        </div>
    </section>
  )
}

export default Experience