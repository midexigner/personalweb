import Styles from '../styles/Footer.module.css'
import Link from 'next/link'
const Footer = ()=>{
    return(
<footer className={Styles.footer}>
    <div className="container">
        <div className={`layout-grid ${Styles.footerGrid}`}>
            <div className={Styles.footerSubmit}>
            <Link href={'/'}className={Styles.brand}><p className={Styles.logo}>MI Dexigner<span className={Styles.dot}>.</span></p></Link>
            </div>
            <div className={Styles.alternative}>
            <ul className={Styles.footerMenu}>
                <li><Link href={'/'}>Home</Link></li>
                <li><Link href={'/services'}>Services</Link></li>
                {/* <li><Link href={'/'}>Pricing</Link></li> */}
                <li><Link href={'/portfolio'}>Portfolio</Link></li>
                {/* <li><Link href={'/'}>Blog</Link></li> */}
                <li><Link href={'/about-us'}>About Us</Link></li>
                <li><Link href={'/contact-us'}>Contact</Link></li>
            </ul>
            </div>
        </div>
        <div className={Styles.footerCopyright}>
            <div className={Styles.footerBottomText}>
                Design &amp; Developed By <Link href='/' className={Styles.footerLink}>MI Dexigner</Link>
            </div>
            <div className={Styles.footerBottom}>
            <div className={Styles.footerBottomText}>
                <Link href="/terms-conditions" className={Styles.footerLink}>Term &amp; Conditions</Link>
                </div>
            <div className={Styles.footerBottomText}><span className={Styles.linkAlternativeSpace}>|</span></div>
            <div className={Styles.footerBottomText}><Link href="/privacy-policy" className={Styles.footerLink}>Privacy Policy</Link></div>
            </div>
        </div>
    </div>
</footer>
    )
}

export default  Footer;