import Styles from '../styles/Work.module.css'
import Link from 'next/link'
import Heading from './Heading'

const Work = () => {
  return (
    <section className='section'>
        <div className='container'>
            <Heading title={'Recently Work'} buttonURL="/portfolio" buttonURLText="View all works" align="left" />
        
<div className={Styles.wDynList}>
<div className={Styles.collectionListProjects}>
    <div className={Styles.wDynItem}>
        <div className={Styles.galleryWrapper}>
           <div className={Styles.gallery}>
               <Link href='/' className={Styles.galleryImage} style={{backgroundImage:`url('/work-01.jpg')`}}></Link>
               <div className={Styles.textHover}>
                   <Link href="/" className={Styles.linkPortfolio}>
                       <h5 className={Styles.titleText}>Marine</h5>
                   </Link>
               </div>
           </div>
        </div>
    </div>
    <div className={Styles.wDynItem}>
        <div className={Styles.galleryWrapper}>
           <div className={Styles.gallery}>
               <Link href='/' className={Styles.galleryImage} style={{backgroundImage:`url('/work-01.jpg')`}}></Link>
               <div className={Styles.textHover}>
                   <Link href="/" className={Styles.linkPortfolio}>
                       <h5 className={Styles.titleText}>Marine</h5>
                   </Link>
               </div>
           </div>
        </div>
    </div>
    <div className={Styles.wDynItem}>
        <div className={Styles.galleryWrapper}>
           <div className={Styles.gallery}>
               <Link href='/' className={Styles.galleryImage} style={{backgroundImage:`url('/work-01.jpg')`}}></Link>
               <div className={Styles.textHover}>
                   <Link href="/" className={Styles.linkPortfolio}>
                       <h5 className={Styles.titleText}>Marine</h5>
                   </Link>
               </div>
           </div>
        </div>
    </div>
    <div className={Styles.wDynItem}>
        <div className={Styles.galleryWrapper}>
           <div className={Styles.gallery}>
               <Link href='/' className={Styles.galleryImage} style={{backgroundImage:`url('/work-01.jpg')`}}></Link>
               <div className={Styles.textHover}>
                   <Link href="/" className={Styles.linkPortfolio}>
                       <h5 className={Styles.titleText}>Marine</h5>
                   </Link>
               </div>
           </div>
        </div>
    </div>
</div>

</div>

        </div>
    </section>
  )
}

export default Work