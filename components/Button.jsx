import Link from 'next/link'
import Styles from '../styles/Button.module.css' 

const Button = ({url,text,type}) => {
  return (
    <Link href={url} className={`${Styles.button} ${type}`}>{text}</Link>
  )
}

export default Button