import React from 'react';
import Heading from './Heading';
import Styles from '../styles/About.module.css';
import Button from './Button';

const About = () => {
  return (
    <section className="section">
      <div className="container">
        <Heading title={'About'} />
        <div className="layout-grid grid two-column">
          <div className={Styles.aboutContentImage} />
          <div className={Styles.aboutInfo}>
            <h2 className="info-title">Boost Your Visibility with Us Regardless of Your Business Size in Pakistan</h2>
            <p>
            MI Dexigner offers an extensive range of digital marketing agency services tailored for both small and large businesses. Our robust content marketing agency services empower you to compete effectively in the market, enhancing your brand's online visibility and attracting potential customers to generate leads and facilitate rapid conversions. We excel in crafting compelling brand messages that inspire customer loyalty, making repeat business a top priority for our clients.
            </p>
            <div className={Styles.buttonWrapper}>
                <Button url={'/about-us'} text={'More Info'} type="button-color" />
                <Button url={'tel:+923242340583'} text={'Get A Quote'} />
              </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default About;
