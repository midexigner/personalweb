export const landingQuery = `*[_type == "landingpage"]{
    home{
    title,
    subtitle,
    subtitle2,
    btnText,
    btnURL,
    banner,
    description,
    abouttitle,
    aboutcontent,
    aboutbtnURL,
    aboutbtnText,
    aboutBanner
  }
  }`;
  
export const partnerQuery = `*[_type == "partner"]{
    _id,
    mainImage
  }`;


export const portfolioQuery = `*[_type == "portfolio" && !(_id in path("drafts.**")) ]| order(_createdAt desc){
    _id,
    image,
  thumbnail,
  title,
  websiteBtnURL
  }[0...6]`;

export const postSlugQuery = `*[_type == "post" && slug.current == $slug][0]{
      _id,
    _type,
    title,
    slug,
    mainImage,
    body,
  category,
     _createdAt
    }`;

export const pageQuery = `*[_type == "page"]{
  _id,
  _type,
  title,
  slug,
  mainImage,
  body
}`;

export const settingQuery = `*[_type == "siteSettings" && !(_id in path("drafts.**")) ]{
    _id,
    logo,
    title,
    tagline,
    url,
    desc,
    keywords,
    country,
    copyright,
  
  }[0]`;

  export const categoryQuery = `*[_type == "category"]| order(_createdAt desc){
    _id,
    _type,
    title,
    slug,
  }`;

export const categorySlugQuery = `*[_type == "category" && slug.current == $slug][0]{
    _id,
    title,
    slug,
   "relatedPosts": *[_type=='post' && references(^._id)]{ 
   title,
   slug,
     mainImage,
  category[]->{ title,slug },
   },
  }`;

  export const postQuery = `*[_type == "post"]| order(_createdAt desc){
    _id,
    _type,
    title,
    slug,
    mainImage,
  category[]->{ title,slug },
  }`;

  export const testimonialQuery = `*[_type == "testimonials"]| order(_createdAt desc){
    _id,
    name,
  subtitle,
  rating,
  content,
  thumbnail
  }`;
