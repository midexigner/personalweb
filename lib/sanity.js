import { 
  createClient, 
  createPreviewSubscriptionHook,
  createImageUrlBuilder,
  createPortableTextComponent
} from "next-sanity";

const config ={
  projectId:process.env.NEXT_PUBLIC_SANITY_PROJECT_ID,
  dataset:process.env.NEXT_PUBLIC_SANITY_DATASET,
  apiVersion:process.env.NEXT_PUBLIC_API_VERSION,
  useCdn:process.env.NEXT_PUBLIC_CDN
}

export const sanityClient = createClient(config)

export const usePreviewSubscription = createPreviewSubscriptionHook(config);

export const urlFor = (source)=>createImageUrlBuilder(config).image(source)

export const PortableText = createPortableTextComponent({
  ...config,
  serializers:{}
})