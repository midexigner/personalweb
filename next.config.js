module.exports = {
  compiler: {
    styledComponents: true,
},
  images: {
    unoptimized: true,
    domains: ["cdn.sanity.io","fs1.extraimage.org","midexigner.com"],
    contentSecurityPolicy: "default-src 'self'; script-src 'none';",
    minimumCacheTTL: 6000,
    //deviceSizes: [640, 750, 828, 1080, 1200, 1920, 2048, 3840]
},
swcMinify: true,
  }

