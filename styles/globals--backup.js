import css from 'styled-jsx/css'

export default css.global`
:root{
  --dark:
  --primary: #00aeee;
  --secondary: #f9f7ff;
  --ternary: #00aeef;
  --shadow:1em 1em 4em rgba(84,103,255,.2);
  --heading:'Righteous', cursive;
  --paragraph:'Poppins', sans-serif;
}
[data-theme=dark] {
  --primary: #5467ff;
  --secondary: #f9f7ff;
  --shadow:1em 1em 4em rgba(84,103,255,.2);
}
*, :after, :before {
  margin: 0;
  padding: 0;
  box-sizing: border-box;
}
/* hide scrollbar for chrome,safaro and opera */
body::-webkit-scrollbar{
    display: none;
}
/* hide scrollbar for IR,Edge and firefox */
body{
   -ms-overflow-style:none;/*IE and Edge*/
   scrollbar-width:none ;/*firefox*/
}
html {
  font-family: sans-serif;
  -ms-text-size-adjust: 100%;
  -webkit-text-size-adjust: 100%;
}
body {
  margin: 0;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}
article,
aside,
details,
figcaption,
figure,
footer,
header,
main,
menu,
nav,
section,
summary {
  display: block;
}
audio,
canvas,
progress,
video {
  display: inline-block;
}
audio:not([controls]) {
  display: none;
  height: 0;
}
progress {
  vertical-align: baseline;
}
[hidden],
template {
  display: none;
}
a {
  background-color: transparent;
  -webkit-text-decoration-skip: objects;
}
a:active,
a:hover {
  outline-width: 0;
}
abbr[title] {
  border-bottom: none;
  text-decoration: underline;
  text-decoration: underline dotted;
}
b,
strong {
  font-weight: inherit;
  font-weight: bolder;
}
dfn {
  font-style: italic;
}
h1 {
  font-size: 2em;
  margin: 0.67em 0;
}
mark {
  background-color: #ff0;
  color: #000;
}
small {
  font-size: 80%;
}
sub,
sup {
  font-size: 75%;
  line-height: 0;
  position: relative;
  vertical-align: baseline;
}
sub {
  bottom: -0.25em;
}
sup {
  top: -0.5em;
}
img {
  border-style: none;
}
svg:not(:root) {
  overflow: hidden;
}
code,
kbd,
pre,
samp {
  font-family: monospace, monospace;
  font-size: 1em;
}
figure {
  margin: 1em 40px;
}
hr {
  box-sizing: content-box;
  height: 0;
  overflow: visible;
}
button,
input,
optgroup,
select,
textarea {
  font: inherit;
  margin: 0;
}
optgroup {
  font-weight: 700;
}
button,
input {
  overflow: visible;
}
button,
select {
  text-transform: none;
}
[type="reset"],
[type="submit"],
button,
html [type="button"] {
  -webkit-appearance: button;
}
[type="button"]::-moz-focus-inner,
[type="reset"]::-moz-focus-inner,
[type="submit"]::-moz-focus-inner,
button::-moz-focus-inner {
  border-style: none;
  padding: 0;
}
[type="button"]:-moz-focusring,
[type="reset"]:-moz-focusring,
[type="submit"]:-moz-focusring,
button:-moz-focusring {
  outline: 1px dotted ButtonText;
}
fieldset {
  border: 1px solid silver;
  margin: 0 2px;
  padding: 0.35em 0.625em 0.75em;
}
legend {
  box-sizing: border-box;
  color: inherit;
  display: table;
  max-width: 100%;
  padding: 0;
  white-space: normal;
}
textarea {
  overflow: auto;
}
[type="checkbox"],
[type="radio"] {
  box-sizing: border-box;
  padding: 0;
}
[type="number"]::-webkit-inner-spin-button,
[type="number"]::-webkit-outer-spin-button {
  height: auto;
}
[type="search"] {
  -webkit-appearance: textfield;
  outline-offset: -2px;
}
[type="search"]::-webkit-search-cancel-button,
[type="search"]::-webkit-search-decoration {
  -webkit-appearance: none;
}
::-webkit-input-placeholder {
  color: inherit;
  opacity: 0.54;
}
::-webkit-file-upload-button {
  -webkit-appearance: button;
  font: inherit;
}
html {
  font: 112.5%/1.45em georgia, serif;
  box-sizing: border-box;
  overflow-y: scroll;
}

body {
  color: hsla(0, 0%, 0%, 0.8);
  font-family: sans-serif;
  font-weight: normal;
  word-wrap: break-word;
  overflow:hidden;
}
img {
  max-width: 100%;
}
h1 {
  margin-left: 0;
  margin-right: 0;
  margin-top: 0;
  padding-bottom: 0;
  padding-left: 0;
  padding-right: 0;
  padding-top: 0;
  margin-bottom: 1.45rem;
  color: inherit;
  font-weight: bold;
  text-rendering: optimizeLegibility;
  font-size: 2.25rem;
  line-height: 1.1;
}
h2 {
  margin-left: 0;
  margin-right: 0;
  margin-top: 0;
  padding-bottom: 0;
  padding-left: 0;
  padding-right: 0;
  padding-top: 0;
  margin-bottom: 1.45rem;
  color: inherit;
  font-weight: bold;
  text-rendering: optimizeLegibility;
  font-size: 1.62671rem;
  line-height: 1.1;
}
h3 {
  margin-left: 0;
  margin-right: 0;
  margin-top: 0;
  padding-bottom: 0;
  padding-left: 0;
  padding-right: 0;
  padding-top: 0;
  margin-bottom: 1.45rem;
  color: inherit;
  font-weight: bold;
  text-rendering: optimizeLegibility;
  font-size: 1.38316rem;
  line-height: 1.1;
}
h4 {
  margin-left: 0;
  margin-right: 0;
  margin-top: 0;
  padding-bottom: 0;
  padding-left: 0;
  padding-right: 0;
  padding-top: 0;
  margin-bottom: 1.45rem;
  color: inherit;
  font-weight: bold;
  text-rendering: optimizeLegibility;
  font-size: 1rem;
  line-height: 1.1;
}
h5 {
  margin-left: 0;
  margin-right: 0;
  margin-top: 0;
  padding-bottom: 0;
  padding-left: 0;
  padding-right: 0;
  padding-top: 0;
  margin-bottom: 1.45rem;
  color: inherit;
  font-weight: bold;
  text-rendering: optimizeLegibility;
  font-size: 0.85028rem;
  line-height: 1.1;
}
h6 {
  margin-left: 0;
  margin-right: 0;
  margin-top: 0;
  padding-bottom: 0;
  padding-left: 0;
  padding-right: 0;
  padding-top: 0;
  margin-bottom: 1.45rem;
  color: inherit;
  font-weight: bold;
  text-rendering: optimizeLegibility;
  font-size: 0.78405rem;
  line-height: 1.1;
}
hgroup {
  margin-left: 0;
  margin-right: 0;
  margin-top: 0;
  padding-bottom: 0;
  padding-left: 0;
  padding-right: 0;
  padding-top: 0;
  margin-bottom: 1.45rem;
}
ul {
  margin-left: 1.45rem;
  margin-right: 0;
  margin-top: 0;
  padding-bottom: 0;
  padding-left: 0;
  padding-right: 0;
  padding-top: 0;
  margin-bottom: 1.45rem;
  list-style-type: outside;
  list-style-type: none;
  list-style-image: none;
  margin:0px;
}
ol {
  margin-left: 1.45rem;
  margin-right: 0;
  margin-top: 0;
  padding-bottom: 0;
  padding-left: 0;
  padding-right: 0;
  padding-top: 0;
  margin-bottom: 1.45rem;
  list-style-position: outside;
  list-style-type: none;
  list-style-image: none;
  margin:0px;
}
dl {
  margin-left: 0;
  margin-right: 0;
  margin-top: 0;
  padding-bottom: 0;
  padding-left: 0;
  padding-right: 0;
  padding-top: 0;
  margin-bottom: 1.45rem;
}
dd {
  margin-left: 0;
  margin-right: 0;
  margin-top: 0;
  padding-bottom: 0;
  padding-left: 0;
  padding-right: 0;
  padding-top: 0;
  margin-bottom: 1.45rem;
}
p {
  margin-left: 0;
  margin-right: 0;
  margin-top: 0;
  padding-bottom: 0;
  padding-left: 0;
  padding-right: 0;
  padding-top: 0;
  margin-bottom: 1.45rem;
}
figure {
  margin-left: 0;
  margin-right: 0;
  margin-top: 0;
  padding-bottom: 0;
  padding-left: 0;
  padding-right: 0;
  padding-top: 0;
  margin-bottom: 1.45rem;
}
pre {
  margin-left: 0;
  margin-right: 0;
  margin-top: 0;
  margin-bottom: 1.45rem;
  font-size: 0.85rem;
  line-height: 1.42;
  background: hsla(0, 0%, 0%, 0.04);
  border-radius: 3px;
  overflow: auto;
  word-wrap: normal;
  padding: 1.45rem;
}
table {
  margin-left: 0;
  margin-right: 0;
  margin-top: 0;
  padding-bottom: 0;
  padding-left: 0;
  padding-right: 0;
  padding-top: 0;
  margin-bottom: 1.45rem;
  font-size: 1rem;
  line-height: 1.45rem;
  border-collapse: collapse;
  width: 100%;
}
fieldset {
  margin-left: 0;
  margin-right: 0;
  margin-top: 0;
  padding-bottom: 0;
  padding-left: 0;
  padding-right: 0;
  padding-top: 0;
  margin-bottom: 1.45rem;
}
blockquote {
  margin-left: 1.45rem;
  margin-right: 1.45rem;
  margin-top: 0;
  padding-bottom: 0;
  padding-left: 0;
  padding-right: 0;
  padding-top: 0;
  margin-bottom: 1.45rem;
}
form {
  margin-left: 0;
  margin-right: 0;
  margin-top: 0;
  padding-bottom: 0;
  padding-left: 0;
  padding-right: 0;
  padding-top: 0;
  margin-bottom: 1.45rem;
}
noscript {
  margin-left: 0;
  margin-right: 0;
  margin-top: 0;
  padding-bottom: 0;
  padding-left: 0;
  padding-right: 0;
  padding-top: 0;
  margin-bottom: 1.45rem;
}
iframe {
  margin-left: 0;
  margin-right: 0;
  margin-top: 0;
  padding-bottom: 0;
  padding-left: 0;
  padding-right: 0;
  padding-top: 0;
  margin-bottom: 1.45rem;
}
hr {
  margin-left: 0;
  margin-right: 0;
  margin-top: 0;
  padding-bottom: 0;
  padding-left: 0;
  padding-right: 0;
  padding-top: 0;
  margin-bottom: calc(1.45rem - 1px);
  background: hsla(0, 0%, 0%, 0.2);
  border: none;
  height: 1px;
}
address {
  margin-left: 0;
  margin-right: 0;
  margin-top: 0;
  padding-bottom: 0;
  padding-left: 0;
  padding-right: 0;
  padding-top: 0;
  margin-bottom: 1.45rem;
}
b {
  font-weight: bold;
}
strong {
  font-weight: bold;
}
dt {
  font-weight: bold;
}
th {
  font-weight: bold;
}

ol li {
  padding-left: 0;
}
ul li {
  padding-left: 0;
}
li > ol {
  margin-left: 1.45rem;
  margin-bottom: calc(1.45rem / 2);
  margin-top: calc(1.45rem / 2);
}
li > ul {
  margin-left: 1.45rem;
  margin-bottom: calc(1.45rem / 2);
  margin-top: calc(1.45rem / 2);
}
blockquote *:last-child {
  margin-bottom: 0;
}
li *:last-child {
  margin-bottom: 0;
}
p *:last-child {
  margin-bottom: 0;
}
li > p {
  margin-bottom: calc(1.45rem / 2);
}
code {
  font-size: 0.85rem;
  line-height: 1.45rem;
}
kbd {
  font-size: 0.85rem;
  line-height: 1.45rem;
}
samp {
  font-size: 0.85rem;
  line-height: 1.45rem;
}
abbr {
  border-bottom: 1px dotted hsla(0, 0%, 0%, 0.5);
  cursor: help;
}
acronym {
  border-bottom: 1px dotted hsla(0, 0%, 0%, 0.5);
  cursor: help;
}
abbr[title] {
  border-bottom: 1px dotted hsla(0, 0%, 0%, 0.5);
  cursor: help;
  text-decoration: none;
}
thead {
  text-align: left;
}
td,
th {
  text-align: left;
  border-bottom: 1px solid hsla(0, 0%, 0%, 0.12);
  font-feature-settings: "tnum";
  -moz-font-feature-settings: "tnum";
  -ms-font-feature-settings: "tnum";
  -webkit-font-feature-settings: "tnum";
  padding-left: 0.96667rem;
  padding-right: 0.96667rem;
  padding-top: 0.725rem;
  padding-bottom: calc(0.725rem - 1px);
}
th:first-child,
td:first-child {
  padding-left: 0;
}
th:last-child,
td:last-child {
  padding-right: 0;
}
tt,
code {
  background-color: hsla(0, 0%, 0%, 0.04);
  border-radius: 3px;
  font-family: "SFMono-Regular", Consolas, "Roboto Mono", "Droid Sans Mono",
    "Liberation Mono", Menlo, Courier, monospace;
  padding: 0;
  padding-top: 0.2em;
  padding-bottom: 0.2em;
}
pre code {
  background: none;
  line-height: 1.42;
}
code:before,
code:after,
tt:before,
tt:after {
  letter-spacing: -0.2em;
  content: " ";
}
pre code:before,
pre code:after,
pre tt:before,
pre tt:after {
  content: "";
}
.blog__wrapper{
  position:relative;
  position: relative;
  padding-top: 150px;
  padding-bottom: 60px;
}
.blog__grid{
  display: grid;
    grid-template-columns:5fr 1.5fr;
    column-gap: 2rem;
}
.blog__grid_list{
  display: grid;
    grid-template-columns:repeat(2,1fr);
    column-gap: 2rem;
}
.heading__blog{
  position:relative;
}
.heading__blog h2 {
  margin-bottom: 20px;
  font-size: 20px;
  line-height: 22px;
  padding: 10px 0;
  color: var(--primary);
  border-top: 3px solid var(--primary);
  border-bottom: 1px solid var(--primary);
  font-weight: 700;
}
.blog__article {
  background-color: #fff;
  display: block;
  padding: 0;
  -webkit-transition: all .25s ease-in-out;
  -moz-transition: all .25s ease-in-out;
  transition: all .25s ease-in-out;
  -webkit-border-radius: 2px;
  -moz-border-radius: 2px;
  border-radius: 5px;
  -webkit-box-shadow: 0 1px 1px #ddd;
  -moz-box-shadow: 0 1px 1px #ddd;
  box-shadow: 0 0 15px rgba(0,0,0,.08);
}

.blog__article:hover {
  -webkit-box-shadow: 0 0 10px #aea6a6;
  -moz-box-shadow: 0 0 10px #aea6a6;
  box-shadow: 0 0 10px #aea6a6; 
}
.blog__article .blog__article-header{
position:relative;
}
.blog__article .blog__article-header img {
  -webkit-border-top-left-radius: 2px;
  -moz-border-top-left-radius: 2px;
  border-top-left-radius: 2px;
  -webkit-border-top-right-radius: 2px;
  -moz-border-top-right-radius: 2px;
  border-top-right-radius: 2px;
}
.catgeory_tag{
 position: absolute;
display: flex;
top: 0px;
z-index: 9;
}
.blog__article .img_cat_top{
  position: relative;
  padding: 5px 10px;
  font-size: 11px;
  border-radius: 0px;
  background: var(--primary) !important;
  box-shadow: 0 0 5px #a1a1a1;
 
}
.blog__article .img_cat_top a{
  color:#fff;
}
.blog__article .img_cat_top a:hover{
  text-decoration:none;
}
.blog__article .blog__article-attrib {
  background-color: #fff;
  padding: 20px 15px;
  font-size: 12px;
  color: var(--custom-theme-text-color);
  height: auto;
  border-radius: 5px;
}

.blog__article.vpost .blog__article-attrib {
  background: #f1f5f9;
  margin-bottom: 10px;
  padding: 10px 10px;
}
.blog__article.vpost .blog__article-header img{
  max-height: 250px !important;
}
.blog__body{
  padding: 0px 20px 0 15px;
    padding-bottom: 0.5rem!important;
    background: transparent;
    width: 100%;
    padding-bottom: 0;
    -webkit-border-bottom-left-radius: 2px;
    -moz-border-bottom-left-radius: 2px;
    border-bottom-left-radius: 2px;
    -webkit-border-bottom-right-radius: 2px;
    -moz-border-bottom-right-radius: 2px;
    border-bottom-right-radius: 2px;
    min-height: 60px;
}
.blog__img{
  position:relative;
  height:250px
}
.blog_title {
  font-size: 18px;
  font-weight: 500;
  line-height: 1.5;
  margin:0px;
}
.blog_title a{
  color:#000;
}
@media only screen and (max-width: 480px) {
  html {
    font-size: 100%;
  }
}
.contact__wrapper{
  position:relative;
  position: relative;
  padding-top: 150px;
  padding-bottom: 60px;
}
.contact__content {
    max-width: 640px;
    margin: 0px auto;
    text-align: center;
    width: 100%;
}
h1.post-title {
  font-size: 3.75rem;
  line-height: 1.3;
  text-align: center;
  margin: 2rem 0 0.5rem 0;
}
.post-entry {
  font-size: 0.9375rem;
  letter-spacing: 0.125rem;
  color: #a29a97;
  text-transform: uppercase;
  text-align: center;
}
h2.post-subtitle {
  font-size: 2.625rem;
  text-align: center;
  margin: 1rem 0 1rem 0;
}
.featured-image {
  width: 100%;
  height: 40.63rem;
  vertical-align: middle;
  margin: 1.5rem 0 3rem 0;
  position: relative;
}
.featured-image > div img{
object-fit: contain !important;
height: 28.63rem !important;
margin: 0px auto !important;
object-position: center !important;
}
.post-content {
  max-width: 80rem;
  width: 100%;
  margin: 0 auto;
}
.post-content h3 {
  font-size: 1.75rem;
}
.post-content  blockquote {
  font-size: 2rem;
  font-style: italic;
  font-weight: 400;
  line-height: 1.3;
  text-align: center;
  margin: 2rem 0;
}
body{
    font-family: 'Poppins', sans-serif;
  line-height:24px;
  font-size:14px;
}
.container {
  width: 90vw;
  max-width: 1170px;
  margin: 0 auto;
}

@media(min-width:1200px){
  .container {
    width: 95vw;
    max-width: 1300px;
    margin: 0 auto;
  }
}
a{
  text-decoration:none;
  transition:all 300ms ease-in-out;
  font-family: 'Poppins', sans-serif;
}
p {
  margin: 0 0 15px;
  font-style: normal;
  line-height: 1.6em;
  font-family: 'Poppins', sans-serif;
}

.text-center{
    text-align:center;
}

.text-left{
    text-align:left;
}
.text-right{
    text-align:right;
}

.text-justify{
    text-align:justify;
}
body{
  scrollbar-width: thin;
  -ms-overflow-style:none;/*IE and Edge*/
  scrollbar-color: var(--primary) var(--secondary);
}
body::-webkit-scrollbar{
  width: 6px;
  height: 6px;
}
body::-webkit-scrollbar-thumb{
 background-color: var(--primary);
}


header {
  position: fixed;
  top: 2rem;
  left: 0;
  width: 100%;
  height: 5rem;
  display: flex;
  align-items: center;
  z-index: 200;
  background: transparent;
  transition:all .5s ease-in-out;
}
.header__fixed{
  position: fixed;
  transition:all .5s ease-in-out;
  top:1.5rem;
  
}
.header__fixed .nav{
  box-shadow: 1em 1em 8em rgba(0,0,0,.2);
}
.nav {
  display: grid;
  grid-template-columns: 340px 1fr auto;
  align-items: center;
  background: #fff;
  padding: 10px 30px;
  border-radius: 999px;
  box-shadow:0 0 0.75rem rgba(0,0,0,.5);
  margin: 0px auto;
}
.logo {
  display: flex;
  justify-content: space-between;
  align-items: center;
  position:relative;
 width: 100%;
max-width: 300px;
min-height: 60px;
height: 100%;
}
.logo img {
  margin-bottom: 0.375rem;
}
.navigation {
  display: none;
}
.navigation {
  display: flex;
  justify-content: flex-end;
}
.navigation li {
  margin-right: 0.25rem;
  padding:.75rem 1rem;
}
.navigation li > a {
  text-transform: uppercase;
  color: #102a42;
  font-weight: 500;
  letter-spacing: 0.2rem;
  transition: all 0.3s linear;
  padding: 0.5rem 0;
  font-size: 0.8rem;
  line-height: 1.5;
  cursor:pointer;
}
.navigation li > a.active,
.navigation li > a:hover {
  color: var(--primary);
  box-shadow: 0 2px var(--primary);
}
.navigation li a:active,
.navigation li a:visited{
  box-shadow: 0 0px var(--primary);
}
.navigation li.hire__me{
  background-color: var(--primary);
  border-radius:999px;
  margin:0;
  padding:.75rem 1.5rem;
  box-shadow: 1em 1em 4em rgba(84,103,255,.2);
}
.navigation li.hire__me > a{
  color: #fff;
}
.navigation li.hire__me > a:hover{
  color: #fff;
  box-shadow: 0 0px #fff;
}

.hero{
    position: relative;
    margin-top: -35px;
    padding-top: 5rem;
    height: 100vh;
    background-color:#f9f7ff;
} 
.hero:before{
  content: '';
    position: absolute;
    width: 0;
    height: 0;
    border-top: 310px solid transparent;
    border-left: 405px solid #ffffff;
    border-bottom: 521px solid transparent;
    right: 0px;
    transform: rotate(52deg);
    top: 22%;
}
.hero:after{
  content:'';
  width:100%;
  height:4px;
  background-color:rgba(84,103,255,.23);
  position:absolute;
  max-width:703px;
  left:1%;
}
  .hero__content{
    height: 100%;
    display: grid;
    align-items: center;
    width: 95vw;
    margin: 0 auto;
    max-width: 1170px;
    grid-template-columns: repeat(12,1fr);
    z-index: 9;
    position: relative;
   
    }
    .hero__content:after{
      content: '';
      max-width: 824px;
      height: 4px;
      background-color: rgba(84,103,255,.23);
      position: absolute;
      width: 100%;
      bottom: 44%;
      right: 0px;
      left: 0px;
      transform: translate(513px, 0px) rotate(142deg);
      z-index:-1;
    }
.hero_caption{
    grid-column: 1/span 6;
    grid-row: 1/1;
    position:relative;
}

.hero_caption h6{
  margin:0px;
  color:var(--primary);
  font-size:0.9rem;
  margin-bottom:0.4rem;
  text-transform:uppercase;
}
.hero_caption h1{
  margin:0px;
  color:#202020;
  font-size:2.5rem;
  margin-bottom:0.4rem;
  text-transform: capitalize;
  font-family: var(--heading);
}
.hero_caption h4{
  margin:0px;
  color:#576069;
  font-size:1rem;
  margin-bottom:0.5rem;
  font-family: var(--heading);
}
.hero_caption p{
  margin:0px;
  color:#576069;
  font-size:0.8rem;
  margin-bottom:0.5rem;
  padding-left:0.4rem;
  border-left:2px solid var(--primary);
}
.hero__content ul{
  margin:0px;
  color:#576069;
  font-size:0.8rem;
  margin-bottom:0.5rem;
  padding-left:0.4rem;
  border-left:2px solid var(--primary);
}
.btn__group{
  margin-top:60px;
}
.btn__link{
  color: #fff;
  background-color: var(--primary);
  font-size: 0.8rem;
  border-radius: 999px;
  margin: 0;
  padding: .75rem 1.5rem;
  display: flex;
  width: 100%;
  max-width: 150px;
  text-align: center;
  justify-content: center;
  box-shadow: 1em 1em 4em rgba(84,103,255,.2);
}

.btn__link:hover{
  box-shadow:5em 0 2em var(--primary) inset;
}
.profile__img{
  position: relative;
  display: block;
  grid-row: 1/1;
  grid-column: 8/13;
} 


.profile__img .inner__img{
 height:500px;
 border-radius:100%;
 position:relative;
 border:10px solid #fff;
 margin-top:36px;
} 
.profile__img .inner__img:before{
  content:"";
  background-image:url('/transpartent-circle.webp');
  background-repeat:no-repeat;
  background-size:cover;
  width: 455px;
    height: 412px;
    position: absolute;
    bottom: 30%;
    right: 11%;
    margin: auto;
    z-index: -1;
}
.profile__img .inner__img > div{
  max-width:100%;
  width:100%;
 max-height: 100%;;
 border-radius:100%;
}
.btn__gradient_outline{
  display:block;
  z-index: 1;
  text-transform: uppercase;
  text-decoration: none;
  text-align: center;
  letter-spacing: 2px;
  line-height: 70px;
  
  font-size: 28px;
}
.btn__gradient_outline:after{
  content: '';
  background: linear-gradient(120deg, #6559ae, #ff7159, #6559ae);
  background-size: 400% 400%;
  -webkit-clip-path: polygon(0% 100%, 4px 100%, 4px 4px, 216px 4px, 216px 66px, 4px 66px, 4px 100%, 100% 100%, 100% 0%, 0% 0%);
  -moz-animation: gradient 3s ease-in-out infinite, border 1s forwards ease-in-out reverse;
  -webkit-animation: gradient 3s ease-in-out infinite, border 1s forwards ease-in-out reverse;
  animation: gradient 3s ease-in-out infinite, border 1s forwards ease-in-out reverse;
}
.btn__gradient_outline > span{
  display: block;
  background: linear-gradient(120deg, #6559ae, #ff7159, #6559ae);
  background-size: 400% 400%;
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;
  -moz-animation: gradient 3s ease-in-out infinite;
  -webkit-animation: gradient 3s ease-in-out infinite;
  animation: gradient 3s ease-in-out infinite;
}
/* motion */
@-moz-keyframes gradient {
  0% {
    background-position: 14% 0%;
  }
  50% {
    background-position: 87% 100%;
  }
  100% {
    background-position: 14% 0%;
  }
}
@-webkit-keyframes gradient {
  0% {
    background-position: 14% 0%;
  }
  50% {
    background-position: 87% 100%;
  }
  100% {
    background-position: 14% 0%;
  }
}
@keyframes gradient {
  0% {
    background-position: 14% 0%;
  }
  50% {
    background-position: 87% 100%;
  }
  100% {
    background-position: 14% 0%;
  }
}
@-moz-keyframes border {
  0% {
    -webkit-clip-path: polygon(0% 100%, 4px 100%, 4px 4px, 216px 4px, 216px 66px, 4px 66px, 4px 100%, 100% 100%, 100% 0%, 0% 0%);
  }
  25% {
    -webkit-clip-path: polygon(0% 100%, 4px 100%, 4px 4px, 216px 4px, 216px 66px, 216px 66px, 216px 100%, 100% 100%, 100% 0%, 0% 0%);
  }
  50% {
    -webkit-clip-path: polygon(0% 100%, 4px 100%, 4px 4px, 216px 4px, 216px 4px, 216px 4px, 216px 4px, 216px 4px, 100% 0%, 0% 0%);
  }
  75% {
    -webkit-clip-path: polygon(0% 100%, 4px 100%, 4px 4px, 4px 4px, 4px 4px, 4px 4px, 4px 4px, 4px 4px, 4px 0%, 0% 0%);
  }
  100% {
    -webkit-clip-path: polygon(0% 100%, 4px 100%, 4px 100%, 4px 100%, 4px 100%, 4px 100%, 4px 100%, 4px 100%, 4px 100%, 0% 100%);
  }
}
@-webkit-keyframes border {
  0% {
    -webkit-clip-path: polygon(0% 100%, 4px 100%, 4px 4px, 216px 4px, 216px 66px, 4px 66px, 4px 100%, 100% 100%, 100% 0%, 0% 0%);
  }
  25% {
    -webkit-clip-path: polygon(0% 100%, 4px 100%, 4px 4px, 216px 4px, 216px 66px, 216px 66px, 216px 100%, 100% 100%, 100% 0%, 0% 0%);
  }
  50% {
    -webkit-clip-path: polygon(0% 100%, 4px 100%, 4px 4px, 216px 4px, 216px 4px, 216px 4px, 216px 4px, 216px 4px, 100% 0%, 0% 0%);
  }
  75% {
    -webkit-clip-path: polygon(0% 100%, 4px 100%, 4px 4px, 4px 4px, 4px 4px, 4px 4px, 4px 4px, 4px 4px, 4px 0%, 0% 0%);
  }
  100% {
    -webkit-clip-path: polygon(0% 100%, 4px 100%, 4px 100%, 4px 100%, 4px 100%, 4px 100%, 4px 100%, 4px 100%, 4px 100%, 0% 100%);
  }
}
@keyframes border {
  0% {
    -webkit-clip-path: polygon(0% 100%, 4px 100%, 4px 4px, 216px 4px, 216px 66px, 4px 66px, 4px 100%, 100% 100%, 100% 0%, 0% 0%);
  }
  25% {
    -webkit-clip-path: polygon(0% 100%, 4px 100%, 4px 4px, 216px 4px, 216px 66px, 216px 66px, 216px 100%, 100% 100%, 100% 0%, 0% 0%);
  }
  50% {
    -webkit-clip-path: polygon(0% 100%, 4px 100%, 4px 4px, 216px 4px, 216px 4px, 216px 4px, 216px 4px, 216px 4px, 100% 0%, 0% 0%);
  }
  75% {
    -webkit-clip-path: polygon(0% 100%, 4px 100%, 4px 4px, 4px 4px, 4px 4px, 4px 4px, 4px 4px, 4px 4px, 4px 0%, 0% 0%);
  }
  100% {
    -webkit-clip-path: polygon(0% 100%, 4px 100%, 4px 100%, 4px 100%, 4px 100%, 4px 100%, 4px 100%, 4px 100%, 4px 100%, 0% 100%);
  }
}
.call__to__action{
  background-color:#f6f7fb;
  background-color:var(--secondary);
  padding:3.75rem 0;
  margin:1.75rem 0px;
  border-top: 3px solid var(--primary);
  border-bottom: 3px solid var(--primary);
  border-radius: 99px 0 0 0;
}
.call__to__action_inner{
  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr;
  column-gap: 2rem;
}
.call__to__action_inner > .icon__item{
  border:3px solid #5467ff;
    align-items: center;
    border-radius: 999px;
    display: flex;
    justify-content: center;
    height: 100%;
    min-height: 300px;
    width: 100%;
    transition: all 0.3s linear;
}
.call__to__action_inner > .icon__item:hover{
  box-shadow: 2px -4px 0px 25px rgba(84,103,255,0.75);
  border:3px solid transparent;
}
.call__to__action_inner > .icon__item > svg{
  width:100%;
}
.call__to__action_inner > .icon__item > svg .text{
 display:none
}
.call__to__action_inner>.icon__item:hover svg .text path {
  text-transform: uppercase;
  -webkit-animation: stroke 2.5s;
  animation: stroke 2.5s;
  stroke-width: 2;
  stroke: #365fa0;
  font-size: 140px;
  fill: #365fa0;
}
.call__to__action_inner>.icon__item:hover svg .text2 path {
  animation: stroke 1s linear forwards;
}
.call__to__action_inner>.icon__item:hover svg .text{
display:block;
fill: rgba(84,103,255,1);
}
.call__to__action_inner>.icon__item:hover svg .icon{
  display:none
}
.call__to__action_inner > .icon__item > svg .icon{
  fill:rgba(84,103,255,1);
  
}
svg .text2 path{
  fill:none;
}
svg .text2 path {
	text-transform: uppercase;
	animation: stroke 2.5s infinite alternate;
	stroke-width: 2;
	stroke: #365fa0;
	font-size: 140px;
}
@keyframes stroke {
	0%   {
		fill: rgba(84,103,255,0); stroke: rgba(84,103,255,1);
		stroke-dashoffset: 25%; stroke-dasharray: 0 50%; stroke-width: 2;
	}
	70%  {fill: rgba(84,103,255,0); stroke: rgba(54,95,160,1); }
	80%  {fill: rgba(84,103,255,0); stroke: rgba(54,95,160,1); stroke-width: 3; }
	100% {
		fill: rgba(84,103,255,1); stroke: rgba(54,95,160,0); 
		stroke-dashoffset: -25%; stroke-dasharray: 50% 0; stroke-width: 0;
	}
}
.about_section{
  padding:6.75rem 0;
  position:relative;
  margin:0px;
}
.about_section:before{
  content:'';
  background: radial-gradient(ellipse at center, #f2f9ff 0%, #f2f9ff 30%, transparent 30%);
  background-size: 20px 20px;
  background-repeat: repeat-x;
  background-position: 5px center;
  width: 100%;
height: 100%;
position: absolute;
background-repeat: repeat;
top:0px;
left:0px;
right:0px;
bottom:0px;
}
.about_section:after {
  content: "";
  position: absolute;
  height: 65%;
  left: 8%;
  right: 0;
  bottom: 0;
  background: #edf6fd;
  z-index: 1;
}
.about_inner{
  display: grid;
  grid-template-columns: 40% 55%;
  column-gap: 6rem;
}
.about__pic{
  position:relative;
  z-index:2;
}
.about__pic:before{
  content: '';
  width: 100%;
  height: 200px;
  background-color: var(--ternary);
  position: absolute;
  top: -32px;
  z-index: -1;
  right: -32px;
}
.about__pic:after{
  content: '';
  width: 100%;
  height: 200px;
  background-color: var(--primary);
  position: absolute;
  bottom: -32px;
  z-index: -1;
  left: -32px;
}


.about__content{
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  z-index: 2;
}
.about__content h2{
  font-family:var(--heading);
  font-weight: 700;
}
.about__content .btn__link{
margin-top:30px;
}
.skills{
  padding:3.75rem 0;
position: relative;
margin:0px;
background: #e0e8ef;
}
.skills:before {
  content: "";
  position: absolute;
  top: 0;
  left: 50%;
  right: 0;
  bottom: 0;
  background: #f9f7ff;
}
.skills h2{
  font-family: var(--heading);
    font-weight: 700;
}
.skills .skills__inner > div{
  padding:3.75rem;
  background:#1f3651;
  position:relative;
  display: grid;
grid-template-columns: repeat(2,1fr);
column-gap:2rem;
}
.skills .skills__inner > div .progress{
  position:relative;
  margin-bottom:24px;
}
.skills .skills__inner > div .progress > .title{
  margin:0 0 18px;
  color:#fff;
  font-size: 0.85028rem;
    line-height: 1.1;
    font-weight:bold;
}

.skills .skills__inner > div .progress__inner {
  height: 20px;
  width: 100%;
  background-color:#eeeeee;
  border-radius: 15px;
  padding: 0px;
  font-size:12px;
  text-align:center;
  overflow:hidden;
  border:none;
}
.skills .skills__inner > div .progress__inner > .progress__bar {
  background: transparent;
  height: 20px;
  color:#fff;
  border-radius: 15px;
  line-height: 20px;
}

.intro__section{
  padding:6.75rem 0;
  padding-bottom: 1rem;
}
.intro_section__inner{
  display: grid;
  grid-template-columns: 42% 58%;
  column-gap: 2rem;
}
.content > h2{
  font-size:2.25rem;
  color:#353535;
  position:relative;
  font-family:var(--heading);
}
.content > h2:after{
  content:'';
  width:100%;
  max-width:76px;
  height:6px;
  display:block;
  background-color:rgba(84,103,255,1);
  border-radius:90px;
  margin-top:22px;
  margin-bottom:15px;
}
.content > p{
  font-size:0.75rem;
  color:#646464;
  font-weight:300;
}

.service__item{
  display:grid;
  grid-template-areas: 'colOne colTwo' 'colThree colFour';
  column-gap: 2rem;
 
}
.service__item li{
  box-shadow:1em 1em 4em rgba(84,103,255,.2);
  background:#fff;
  border-radius:15px;
  position:relative;
}
.service__item li:nth-child(1){
  grid-area: colOne;
  top:108px;
}
.service__item li:nth-child(2){
  grid-area: colTwo;
}
.service__item li:nth-child(3){
  grid-area: colThree;
  top:116px;
}
.service__item li:last-child{
  grid-area: colFour;
}
.service__item li + li + li{
  margin-top:48px;
}
.service__item li article{
  padding-left:30px;
  padding-right:30px;
  padding-bottom:50px;
  padding-top:18px;
}
.service__item li article span{
  position: relative;
width: 90px;
height: 90px;
display: block;
margin-bottom: 20px;
}
.service__item li article h3{
  font-size:1rem;
  font-weight:600;
  font-family:var(--heading);
}
.intro__sections{
  position:relative;
}
.intro__sections:after{
  content: '';
background: #f9f9fb;
position: absolute;
right: 0;
width: 100%;
height: 100%;
}
.workSection{
  padding:3.75rem 0;
}
.heading{
  text-align:center;
  margin-bottom:60px;
}
.heading h2{
  font-size: 2.25rem;
  color: #353535;
  position: relative;
  font-family:var(--heading);
}
.workSection .filterNav{
  display:flex;
  justify-content: center;
margin-bottom: 20px;
}
.workSection .filterNav ul{
  display:flex;
}
.workSection .filterNav ul li + li{
  margin-left:30px;
}
.workSection .filterNav ul li a{
  color: #7a8baa;
  text-align: center;
  text-transform: uppercase;
  font-weight:500;
  font-size:1rem;
}
.workSection .filterNav ul li a.active,
.workSection .filterNav ul li a:hover{
  color:var(--primary);
 
}
.workSection .grid{
  display: grid;
  grid-template-columns: repeat(4, 1fr);
column-gap: 2rem;
}

.workSection .grid article{
  position:relative;
  margin-bottom:25px;
  overflow:hidden;
  transition: all .2s !important;
}
.workSection .grid article:nth-of-type(1){
  grid-column: 1 / 3;
}
.workSection .grid article:nth-of-type(6){
  grid-column: 3 / 5;
}
.workSection .grid article > .thumbnail{
  height:300px;
}
.workSection .grid article img{
  transform: scale(1.5) rotate(15deg);
  transition: all .2s !important;
}
.workSection .grid article:hover img{
  transform: scale(1.0) rotate(0deg);
}
.workSection .grid article .caption{
  position:absolute;
  top:0px;
  right:0px;
  left:0px;
  bottom:0px;
  color:#fff;
  display: flex;
justify-content: center;
align-items: center;
flex-direction: column;
background:rgba(0,0,0,.7);
transition: all .2s !important;
transform: translate(250px,350px);

}
.workSection .grid article:hover .caption{
  transform: translate(0px,0px);
}
.workSection .grid article:hover .caption h4{
  font-size: 1.62671rem;
    line-height: 1.1;
}
.partners{
  padding:2.75rem 0;
  background:#e0e8ef;
}
.partners ul{
  justify-content:space-between;
  display: grid;
  grid-template-columns: repeat(8,1fr);
}
.partners ul li{
  position:relative;
  padding: 0px 10px;
  height:65px;
}
.partners ul li img{
  object-fit:contain;
}
.testimonials{
  background:#ffffff;
  padding:6.75rem 0;
}
.testimonials_section__inner {
  display: grid;
  grid-template-columns:20% 80%;
  -webkit-column-gap: 2rem;
  column-gap: 2rem;
}
.testiLeft h2{
  font-size:45px;
  line-height:50px;
  font-weight:700;
  color:#323232;
  font-family:var(--heading);
}
.testiLeft .swiper-pagination .swiper-pagination-bullet{
  background:#f0f4ff;
  width:11px;
  height:11px;
  opacity:1;
  transition:all .5s ease-in-out;
}
.testiLeft .swiper-pagination .swiper-pagination-bullet + .swiper-pagination-bullet{
  margin-left:15px;
}
.testiLeft .swiper-pagination .swiper-pagination-bullet.swiper-pagination-bullet-active{
  background:var(--primary);
  width:22px;
border-radius:25px;
}
.testiRight .swiper-slide{
  padding-bottom: 20px;
  padding-right:20px;
}
.testiRight .textimonialCard{
  background: #fff;
  box-shadow: .6em .6em .6em rgba(84,103,255,.2);
  padding: 60px 40px;
  border: 1px solid #f5f2f2;
  border-radius: 15px;
}
.testiRight .textimonialCard .testihead{
 display:flex;
 align-content: center;
align-items: center;
margin-bottom:22px;
}
.testiRight .textimonialCard .testihead .testiImage{
 display:block;
 align-content:center;
 border-radius: 999px;
overflow: hidden;
position:relative;
flex:0.2;
}
.testiRight .textimonialCard .testihead .testiHeading{
  display:flex;
  flex:0.8;
 align-content:center;
 flex-direction:column;
 margin-left:25px;
}
.testiRight .textimonialCard .testihead h3{
  margin: 0px;
font-weight: 500;
font-size: 22px;
line-height: 22px;
}
.testiRight .textimonialCard .testihead h6{
  margin: 0px;
font-weight: 400;
font-size: 15px;
line-height: 18px;
color:#909090;
}
.testiRight .textimonialCard .testiDescription p{
  margin: 0px;
font-weight: 300;
font-size: 14px;
line-height: 18px;
color:#909090;
}
footer{
  padding:.75rem .25rem;
  background-color:var(--primary);
}
.footer__inner{
display:grid;
grid-template-columns: repeat(3,1fr);
column-gap: 2rem;
align-items: center;
}
.footer__inner > div{
  color:#fff;
}
.footer-logo {
 position: relative;
width: 100%;
max-width: 300px;
min-height: 60px;
height: 100%;
}
.footer-logo > div img{
  filter: brightness(0) invert(1) !important;
}
.footer_right{
  justify-content:end;
display: flex;
}
.social_media{
  display:flex;
  align-items: center;
}
.social_media > .item + .item{
  margin-left:20px;
}
.social_media > .item > a > svg > path{
  fill:rgba(255, 255, 255, 1);
  transition: all 0.3s linear;
}
.social_media > .item > a:hover svg > path{
  fill:rgba(255, 255, 255, .5);
}
.menu_icons {
  display: flex;
}
.mobile_menu_icon{display:none;}
.dark_mode_theme{
  width:25px;
  height:25px;
  display: flex;
justify-content: center;
align-content: center;
align-items: center;
cursor:pointer;
}
.dark_mode_theme .day_mode,
.dark_mode_theme .night_mode{
display:none;
}
.dark_mode_theme .day_mode.mode_active,
.dark_mode_theme .night_mode.mode_active{
display:flex;
}
.dark_mode_theme:hover{
  color:var(--primary);
}
.portfolio__wrapper{
  display:flex;
  height:100vh;
  overflow:hidden;
  background-color:#38444b;
}

.portfolio__wrapper .text{
  width:50%;
  height:100vh;
  padding:15p 15px 15px 100px;
  display:flex;
  align-items:center;
}

.portfolio__wrapper .text h1{
  font-size:70px;
  line-height:84px;
  font-weight:normal;
  color:#ffe63e;
  text-transform:uppercase;
}
.portfolio__wrapper .text p{
  font-size:20px;
  color:#fbf7db;
  text-transform:uppercase;
  margin-top:15px;
  letter-spacing:4px;
}
.portfolio__wrapper .text .btn{
  position:relative;
  display:inline-block;
  width:100%;
  width:250px;
  height:70px;
  line-height:70px;
  text-align:center;
  border:1px solid #ffe63e;
  color:#ffe63e;
  letter-spacing:4px;
  font-size:16px;
  text-transform:uppercase;
  text-decoration:none;
  margin-top:80px;
  overflow:hidden;
  transition:all ease 0.5s;
  z-index:1;
}
.portfolio__wrapper .text .btn:before{
  content:'';
  position:absolute;
  top:0px;
  left:-100%;
  width:100%;
  height:100%;
  background-color:#ffe63e;
  transition:all ease 0.5s;
  
}
.portfolio__wrapper .text .btn:hover{
  font-weight:700;
  color:#38444b;
}
.portfolio__wrapper .text .btn:hover:before{
left:0px;
}

.portfolio__wrapper .img{
  width:50%;
  padding:0 30px;
  display:flex;
  justify-content:space-between;
}
.portfolio__wrapper .img img{
  width:100%;
  margin:15px;
}
.portfolio__wrapper .img .img-stripe{
width:calc(50% - 50px);
}
.portfolio__wrapper .img .stripe1{
  animation:stripe-one 60s ease 0s infinite;
}
.portfolio__wrapper .img .stripe2{
  transform:translateY(-200%);
  animation:stripe-two 60s ease 0s infinite;
}
@keyframes stripe-one{
  50%{
    transform:translateY(-200%);
  }
}
@keyframes stripe-two{
  50%{
    transform:translateY(0%);
  }
}
@media (max-width:1600px){
 
 .hero svg {
   height: 894px;
 }
}
@media (max-width:1440px){
  .hero{
  
  }
  .profile__img {
    
}
.hero svg {
  height:845px;
   width:auto;
}
.container {
  width: 92vw;
}
}

@media (max-width:1366px){
  .container {
    width: 90vw;
    max-width: 1140px
  }
  


  .call__to__action_inner > .icon__item{
    max-width: 250px;
    min-height: 250px;
    max-height: 250px;
  }
  .testiLeft h2 {
    font-size: 35px;
    line-height: 40px;
}
.workSection .grid article .caption h2{
  font-size: 1.2rem;
}


}
@media(max-width:1024px){
  .container {
    width: 80vw;
    max-width: 960px;
}
.nav{
  display:flex;
  justify-content:space-between;
}
.dark_mode_theme,
  .navigation{display:none;}
  .mobile_menu_icon {
    display: flex;
    font-size: 1.5em;
    cursor: pointer;
    transition: all .1s ease-in-out;
}
.mobile_menu_icon:hover{
  color:var(--primary)
}
  .hero{
    background:#f9f8ff;
  }
  .hero svg {
   display:none;
}
.profile__img {
  order: 2;
}
.hero_caption{
  order: 1;
}
.service__item li article {
  padding: 18px;
}
.service__item li article h3 {
  font-size: .9rem;
  font-family:var(--heading);
}
.content > h2 {
  font-size: 2rem;
}
.intro_section__inner {
  grid-template-columns: 50% 58%;
  column-gap: 1.5rem;
}
.call__to__action_inner > .icon__item {
  max-width: 180px;
  min-height: 180px;
  max-height: 180px;
}
.workSection .grid {
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  column-gap: 1rem;
}
}

@media (max-width:800px){
  .hero__content{
    grid-template-columns: 1fr;
    position:relative;
  }
  .hero {
    height: auto;
}
  .hero_caption {
    height: 65vh;
}
.intro_section__inner {
  grid-template-columns: 100%;
  column-gap: 1rem;
}
.call__to__action_inner {
  grid-template-columns: 1fr 1fr;
  column-gap: 1rem;
}
.call__to__action_inner > .icon__item {
  max-width: 250px;
  min-height: 250px;
  max-height: 250px;
  margin-left:auto;
  margin-right:auto;
  margin-bottom:25px;
}
.workSection .grid {
  grid-template-columns: 1fr 1fr;
}
.testimonials_section__inner {
  grid-template-columns: 100%;
  column-gap: 1rem;
}
.swiper-pagination{
  position:relative;
  margin-bottom: 15px;
}
.footer__inner {
  grid-template-columns:1fr;
  column-gap: 1rem;
}
.footer-logo,
.copyright,
.footer_right{
  justify-content:center;
  text-align:center;
}
.footer-logo{order:1}
.footer_right{order:2}
.copyright{order:3;}
.intro_section__inner .content{margin-bottom:20px;}
.about_inner{
  grid-template-columns:100%;
}

}

@media(max-width:576px){
  .hero_caption {
    height: 85vh;
  padding-left:5%;
  padding-right: 5%
}
.service__item {
  display: grid;
  grid-template-areas: 'colOne' 'colTwo' 'colThree'' colFour';
  column-gap:1rem;
}
.service__item li:nth-child(3),
.service__item li:nth-child(1){
  top: 0px;
}
.service__item li + li + li {
  margin-top: 25px;
}
.service__item li + li{
  margin-top:25px;
}
.workSection .grid,
.call__to__action_inner {
  grid-template-columns: 1fr;
}
.about_section{
  padding: 1.75rem 0;
  margin: 0px
}
.testimonials,
.workSection,
.intro__section{
  padding: 1.75rem 0;
}
.about__pic{
  order:2
}
.about__content{
  order:1
}
.partners ul {
  justify-content: center;
  flex-direction: column;
  align-content: center;
  align-items: center;
}
.testiRight .textimonialCard .testihead{
  flex-direction:column;
  align-items:flex-start;
  margin-bottom:10px;
}
.testiRight .textimonialCard .testihead .testiHeading{
  margin-left:0px;
}
.testiRight .textimonialCard .testihead .testiImage{
 width:60px; 
 margin-bottom:15px;
}
.profile__img > div > div{
  display:none !important;
}
.profile__img{
  min-height: auto;
}
.profile__img img{
  margin-top: -1px !important;
position: relative !important;
}
}

@media (max-width:375px){
  .hero_caption {
    padding-top:35%;
  }
}

// mobile layout styling
.layout__mobile::-webkit-scrollbar {
  display: none;
}
.header__mobile_top {
  margin: 0 15px;
  position: absolute;
  top: 20px;
  width: 100%;
  min-height: 75px;
  z-index:99;
}
.layout__mobile{
  overflow:hidden;
  position:relative;
  background-color:#E3E3E3;
  -ms-overflow-style:none;/*IE and Edge*/
  scrollbar-width:none ;/*firefox*/
}

.mobile__content {
  height: 100vh;
}
.navbar__fixed_onMobile{
position:fixed;
width:100%;
bottom:0px;
left:0px;
right:0px;
margin:0px auto;
}
.navbar__fixed_onMobile ul{
  display:flex;
  padding-left:0px;
  margin:0 15px;

}
.navbar__fixed_onMobile ul li{
  flex:1;
  background-color:#fff;
}
.navbar__fixed_onMobile ul li + li{
margin-left:2px;
}
.navbar__fixed_onMobile ul li a{
  display:flex;
  flex-direction:column;
  flex-direction: column;
text-align: center;
justify-content: center;
align-items: center;
padding: 15px 0px;
color: #000000;
font-size: 14px;
text-transform: uppercase;

}
.home__banner{
  width:100%;
  height:70vh;
  display:flex;
align-content: center;
flex-direction: column;
justify-content: center;
}
.bgWrap {
  position: fixed;
  height: 70vh;
  width: 100vw;
  overflow: hidden;
  z-index: 1;
}
.home__banner .hero_caption {
  padding-top: 0;
  text-align: center;
  height: 100%;
  align-items: center;
  z-index: 9;
}
.home__banner .hero_caption *{
  color:#fff;
}
.home__banner .hero_caption h1 {
  font-size: 2rem;
}
.home__banner .hero_caption p{
  padding-left:0rem;
border-left: 0px solid var(--primary);
padding-top: 0.4rem;
border-top: 2px solid var(--primary);
}
.layout__mobile main{
  margin: 0 15px;
  background-color: #F4F4F6;
}
`