import '../styles/globals.css'
import Header from '../components/Header'
import Footer from '../components/Footer'

export const metadata = {
  metadataBase: new URL('https://midexigner.com'),
  title: 'Professional Web Developer By MI Dexigner',
  tagline: 'Full Stack Developer',
  keywords: 'website design services, web development services, wordpress page builder, custom web development services, professional web development services, website design & development services',
  description: 'I’m a full-stack developer, trainer, and base in Karachi from Pakistan and working as a Senior staff for Ocean Software Pvt Ltd',
  alternates: {
    canonical: `/`,
  },
  openGraph: {
    title: 'Professional Web Developer By MI Dexigner',
    description: 'I’m a full-stack developer, trainer, and base in Karachi from Pakistan and working as a Senior staff for Ocean Software Pvt Ltd',
    images:'https://scontent.fkhi2-2.fna.fbcdn.net/v/t39.30808-6/292692466_414060420745987_946441481661781475_n.jpg?_nc_cat=103&ccb=1-7&_nc_sid=09cbfe&_nc_ohc=Dktsyt5k4r8AX_dy1Av&_nc_ht=scontent.fkhi2-2.fna&oh=00_AT8b43XOI5E2mwzjDTxXrD2CcI7NtAJH321AzfkcviSJ5w&oe=6347E33B',
    url:'https://midexigner.com',
    siteName: 'MI Dexigner',
  },
  robots: {
    index: true,
    follow: true,
    nocache: true,
    googleBot: {
      index: true,
      follow: true,
      noimageindex: false,
      'max-video-preview': -1,
      'max-image-preview': 'large',
      'max-snippet': -1,
    },
  },
  

}

export default function RootLayout({ children }) {
 return (
    <html lang="en">
      <body>
      <div className="page-wrapper">
      <div className="line-wrapper"><div className="line-vertical first"></div><div className="line-vertical"></div><div className="line-vertical"></div><div className="line-vertical"></div><div className="line-vertical"></div><div className="line-vertical"></div><div className="line-vertical none"></div><div className="line-vertical none"></div><div className="line-vertical none"></div><div className="line-vertical none"></div></div> 
      <Header />
        {children}
        <Footer />
        </div>
        </body>
    </html>
  )
}
