import PageHeader from "../../../components/PageHeader";

const PrivacyPolicy = () => {
    return (<>
    <PageHeader title={"Privacy Policy"} url={"privacy-policy"}  />
    <section className="section padding-top">
    <div className="container">
    <p>At MI Dexigner, safeguarding the privacy of our clients and website visitors is paramount. Our Privacy Policy elucidates the data we gather, its utilization, and the measures we take to ensure its security. By engaging with our website or services, you agree to the collection, utilization, and disclosure of your personal information as outlined in this Privacy Policy.</p>
    <h2>Information We Collect</h2>
    <h3>Personal Information</h3>
    <p>When utilizing our website or services, we might gather personal details like your name, email address, phone number, and other contact information. Additionally, we may collect your IP address, browser type, and further details about your computer or device.</p>
    <h3>Non-Personal Information</h3>
    <p>Additionally, we may gather non-personal data, including your browsing history, the pages you access on our website, and the links you interact with. We may employ cookies or similar tracking technologies for this purpose.</p>
    <h3>Payment Information</h3>
    <p>If you complete a purchase or payment via our website, we may gather your credit card number, billing address, and any other necessary information to facilitate the transaction.</p>
    <h2>How We Use Your Information</h2>
    <h3>To provide and improve our services</h3>
    <p>We utilize the gathered information to deliver and enhance our services, which includes analyzing website usage and traffic, offering customer support, and addressing your inquiries.</p>
    <h3>To communicate with you</h3>
    <p>We might utilize your personal information to reach out to you, whether it's to provide details about our services or to respond to your inquiries.</p>
    <h3>To personalize your experience</h3>
    <p>We might utilize your information to tailor your experience on our website, presenting content or advertisements that align more closely with your interests.</p>
    <h3>To send you marketing communications</h3>
    <p>We may utilize your information to deliver marketing communications, such as emails or newsletters, regarding our services or other products and services that might catch your interest.</p>
    <h3>To process payments</h3>
    <p>We might utilize your payment details to handle transactions conducted through our website. Cookies, small files stored on your computer or device when visiting our website, play a role in enhancing your experience. They remember your preferences and settings, as well as track your browsing history to display content or ads tailored to your interests. You can manage cookies on our website by adjusting your browser settings, but disabling them might restrict your access to certain features. Your usage of our website implies consent to cookies as outlined in this Privacy Policy.</p>
    <h3>How We Protect Your Information?</h3>
    <p>We employ industry-standard security measures to safeguard your personal information against unauthorized access, use, or disclosure. At MI Dexigner, we consistently review and update these security protocols to ensure your data's protection. Furthermore, we do not share or sell your personal information to third parties for marketing or any other purposes. Any disclosure of your personal information will only occur as mandated by law or in response to a valid legal request, such as a court order.</p>
    <p>To maintain control over our customers' and viewers' information, SEO websites typically implement various security measures and protocols. These measures may encompass encryption, secure servers, access control, regular security audits, and more. At MI Dexigner, we prioritize encrypting sensitive data like credit card numbers and login credentials to prevent unauthorized access.</p>
    <p>Customer information at MI Dexigner is stored on secure servers shielded by firewalls and other security technologies. Additionally, we enforce access controls to limit access to customer information to authorized personnel only. Regularly reviewing and auditing the security of our systems and networks to detect and address potential vulnerabilities remains our paramount responsibility.</p>
    <h3>Accessing, Changing, and Deleting Your Information</h3>
    <p>You retain the right to access, modify, or erase the personal information we've gathered. Should you wish to exercise these rights, please reach out to us, and we'll assist you accordingly. Additionally, you have the option to opt out of receiving marketing communications from us. If you wish to unsubscribe from our marketing communications, you can do so by clicking the unsubscribe link in the email or by contacting us directly.</p>
    <p>If you have any concerns regarding how we manage your personal information, feel free to contact us, and we will investigate the issue promptly.</p>
    <h3>Changes to This Privacy Policy</h3>
    <p>We may periodically update this Privacy Policy to reflect changes in our practices or to comply with legal requirements. Any updates will be posted on our website and will take effect immediately upon posting. By continuing to use our website or services after any changes to this Privacy Policy, you agree to the collection, use, and disclosure of your personal information as outlined in the updated policy.</p>
    <h3>Contact Us</h3>
    <p>If you have any inquiries regarding this Privacy Policy or our privacy procedures, feel free to contact us via email or phone. You can find our contact information on our website. Your usage of our website or services implies your consent to the collection, utilization, and disclosure of your personal information in accordance with this Privacy Policy.</p>
    <h3>Consumer Data Safety Measures</h3>
    <p>We uphold your right to privacy. Therefore, we do not disclose your name and contact details to any third party, and we treat the information you provide as confidential. Your provided information is solely used to understand your needs and enhance our services accordingly. We adhere to PCI standards and consumer data protection regulations.</p>
    <p>Our company strictly prohibits its representatives from collecting confidential information such as credit card details from customers. Hence, we advise customers against sharing any sensitive information with our employees. Any such sharing is done at the customer's own risk, and our company cannot be held liable for any misuse.</p>
    <p>For quality delivery, your ordered work may be forwarded to any of our production or service centers worldwide. However, these centers are also bound by a Non-Disclosure Agreement (NDA) to maintain the confidentiality of customer information.</p>
    <h3>Pseudonym Policy</h3>
    <p>We implement a pseudonym policy for the following reasons:</p>
    <p>To prevent confusion and inconvenience for our customers when they are assigned a new account manager, we use pseudonyms. This ensures that customers maintain a consistent point of contact and can easily remember their manager's name.</p>
    <p>Additionally, as our staff comes from diverse regions and cultures, using pseudonyms helps us maintain a unified organizational culture.</p>
    <h2>Privacy Policy for Collection of Email Addresses and Mobile Numbers</h2>
    <ol>
        <li>
            <h3>Introduction</h3>
            <p>This Privacy Policy outlines how MI Dexigner gathers, utilizes, and safeguards the email addresses and mobile numbers submitted by users on our website. We are dedicated to safeguarding your privacy and adhering to all applicable data protection regulations.</p>
        </li>
        <li>
            <h3>Information We Collect</h3>
            <p>We might gather the following information:</p>
            <ul>
                <li>Mobile phone numbers</li>
                <li>Any additional information voluntarily provided by the user.</li>
            </ul>
        </li>
        <li>
            <h3>How We Use the Information</h3>
            <p>We utilize the collected mobile numbers for the following purposes:</p>
            <ul>
                <li>Sending relevant updates regarding our services.</li>
                <li>Notifying users about account-related information.</li>
                <li>Internal record keeping.</li>
            </ul>
        </li>
        <li>
            <h3>Consent for Mobile Number Collection</h3>
            <p>By providing your mobile number on MI Dexigner, you agree to the collection and use of this information as detailed in this Privacy Policy.</p>
        </li>
        <li>
            <h3>Security</h3>
            <p>We are dedicated to ensuring the security of your information. To prevent unauthorized access or disclosure, we have implemented appropriate physical, electronic, and managerial procedures to safeguard and secure the information we gather online.</p>
        </li>
        <li>
            <h3>Controlling Your Personal Information</h3>
            <p>You have the option to limit the collection or utilization of your mobile number in the following manners:</p>
            <p>If you had previously consented to us using your mobile number for direct marketing purposes, you can change your decision at any time by contacting us at <a href="mailto:support@midexigner.com">Email Us Here</a> Alternatively, you can reply with "STOP" or "Unsubscribe" to cease receiving messages.</p>
        </li>
        <li>
            <h3>Sharing Your Information</h3>
            <p>We will not sell, distribute, or lease your personal information to third parties unless we have your permission or are legally obligated to do so.</p>
        </li>
        <li>
            <h3>Your Rights</h3>
            <p>You have the right to request information about the personal data we hold regarding you. If you wish to obtain a copy of this information, please contact us at <a href="mailto:support@midexigner.com">Email Us Here.</a></p>
        </li>
        <li>
            <h3>Changes to this Policy</h3>
            <p>MI Dexigner may revise this policy periodically by updating this page. We recommend checking this page regularly to ensure that you are comfortable with any changes.</p>
        </li>
    </ol>
    </div>
    </section>
    </>);
}
 
export default PrivacyPolicy;