import Styles from '../../../styles/Work.module.css'
import PageHeader from '../../../components/PageHeader'

const Portfolio = () => {
  return (
    <>
        <PageHeader title={"Recent Work"} url={"portfolio"} />
        <section className='section padding-top'>
        <div className='container'>
<div className={Styles.wDynList}>
<div className={Styles.collectionListProjects}>
    <div className={Styles.wDynItem}>
        <div className={Styles.galleryWrapper}>
           <div className={Styles.gallery}>
               <a href='/' className={Styles.galleryImage} style={{backgroundImage:`url('/work-01.jpg')`}}></a>
               <div className={Styles.textHover}>
                   <a href="/" className={Styles.linkPortfolio}>
                       <h5 className={Styles.titleText}>Marine</h5>
                   </a>
               </div>
           </div>
        </div>
    </div>
    <div className={Styles.wDynItem}>
        <div className={Styles.galleryWrapper}>
           <div className={Styles.gallery}>
               <a href='/' className={Styles.galleryImage} style={{backgroundImage:`url('/work-01.jpg')`}}></a>
               <div className={Styles.textHover}>
                   <a href="/" className={Styles.linkPortfolio}>
                       <h5 className={Styles.titleText}>Marine</h5>
                   </a>
               </div>
           </div>
        </div>
    </div>
    <div className={Styles.wDynItem}>
        <div className={Styles.galleryWrapper}>
           <div className={Styles.gallery}>
               <a href='/' className={Styles.galleryImage} style={{backgroundImage:`url('/work-01.jpg')`}}></a>
               <div className={Styles.textHover}>
                   <a href="/" className={Styles.linkPortfolio}>
                       <h5 className={Styles.titleText}>Marine</h5>
                   </a>
               </div>
           </div>
        </div>
    </div>
    <div className={Styles.wDynItem}>
        <div className={Styles.galleryWrapper}>
           <div className={Styles.gallery}>
               <a href='/' className={Styles.galleryImage} style={{backgroundImage:`url('/work-01.jpg')`}}></a>
               <div className={Styles.textHover}>
                   <a href="/" className={Styles.linkPortfolio}>
                       <h5 className={Styles.titleText}>Marine</h5>
                   </a>
               </div>
           </div>
        </div>
    </div>
</div>

</div>

        </div>
    </section>
    </>
  )
}

export default Portfolio