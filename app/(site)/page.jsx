import About from "../../components/About";
// import Blog from "../../components/Blog";
import Experience from "../../components/Experience";
import Hero from "../../components/Hero";
// import Partners from "../../components/Partners";
import Service from "../../components/Service";
// import Testimonials from "../../components/Testimonials";
import Video from "../../components/Video";
import Work from "../../components/Work";

const Home = () => {
  return (
    <>
      <Hero />
      <Service />
      <About />
      <Work />
      <Video />
      <Experience />
      {/* <Testimonials/> */}
      {/* <Blog/>*/}
      {/* <Partners /> */}
    </>
  );
};

export default Home;
