import PageHeader from "../../../components/PageHeader";

const TermsCondition = () => {
    return (<>
    <PageHeader title={"Terms and Condition"} url={"terms-conditions"}  />
    <section className="section padding-top">
    <div className="container">
    <p>By utilizing the services offered by our company, you are consenting to abide by the subsequent terms and conditions:</p>
    <p>Our company specializes in providing search engine optimization (SEO) services aimed at enhancing the visibility and ranking of websites on search engines such as Google, Bing, and Yahoo. Employing various strategies like keyword research, link building, and content optimization, we endeavor to boost websites' rankings on search engine result pages (SERPs). Our services are structured to augment your website's traffic and visibility, thereby potentially increasing leads, sales, and revenue.</p>
    <p>MI Dexigner reserves the right to modify these terms of use at any time without prior notice. Such modifications become effective immediately upon being posted on this website. Additionally, MI Dexigner retains the authority to terminate your access to this website at any given time. Your access may be subject to monitoring by MI Dexigner. If accessing this website as a representative of an organization, these terms of use apply both individually and to the organization.</p>
    <p>While MI Dexigner maintains copyright protection over all materials, information, and publications on this website, permission is granted to download, copy, and distribute such information for non-commercial purposes within your organization. In accepting this permission, you agree to preserve all copyright and proprietary notices intact and refrain from altering the website information in any manner. Except where your usage falls under 'fair use' according to copyright law, you are prohibited from utilizing, downloading, uploading, copying, printing, displaying, performing, reproducing, publishing, or distributing any website information without prior written consent from MI Dexigner.</p>
    <p>Full payment is requisite before commencing work on any project. We accept various forms of payment, including credit cards, PayPal, and bank transfers. Refunds are not offered for completed services. In the event of client dissatisfaction with our services' outcomes, we commit to collaborating with the client to implement revisions and enhancements until satisfaction is achieved. Any additional costs or expenses incurred during the SEO project, such as advertising purchases or third-party tools, will be separately invoiced and must be settled in full before work continues.</p>
    <p>We cannot guarantee specific rankings or positions on SERPs as these are influenced by numerous factors beyond our control. Nonetheless, we dedicate ourselves to improving your website's ranking and provide regular progress reports to illustrate the progress of our efforts. We engage with you to comprehend your business and target audience, tailoring an SEO strategy to your specific requirements.</p>
    <p>You are permitted to link to our website's homepage with prior written consent, though linking to any other page necessitates similar consent, which may be revoked at any time. When linking to our website, refrain from using our proprietary logos, marks, or other distinctive graphics, video, or audio material. Additionally, your link must not imply affiliation with, endorsement by, or sponsorship from MI Dexigner, nor cause confusion, mistake, or deception, dilute our trademarks, service marks, or trade names, or contravene applicable law.</p>
    <p>Please be aware that information regarding products, programs, or services on our website may not be available in your country. We recommend consulting with a representative of MI Dexigner to ascertain the availability of products, programs, and services to you.</p>
    <p>Any downloadable software on our website is the copyrighted property of MI Dexigner and/or its suppliers, governed by the terms of the respective license agreement and associated warranties. Similarly, the terms of agreement for any other purchased product or service, in connection with our website, dictate its usage and any associated warranties.</p>
    <p>We pledge not to disclose confidential client information to third parties without express written consent, encompassing details about the client's business, products, or services, as well as any SEO strategies or techniques employed on the client's website. We prioritize the security and privacy of our clients' information, undertaking all necessary measures to safeguard it.</p>
    <p>Certain information on this website originates from other parties, primarily concerning those parties. You acknowledge that such information, encompassing data, text, software, music, sound, photographs, graphics, video, messages, or other materials, is the sole responsibility of the originating party. MI Dexigner disclaims responsibility for such content and does not guarantee its accuracy, integrity, or quality. References or links to other parties' statements or websites should not be construed as endorsement of said parties, their products, or their services.</p>
    <p>Restricted product information available solely to registered customers of MI Dexigner, who have received passwords from the company, is considered confidential. If you are a registered customer or representative thereof, MI Dexigner grants permission to utilize this restricted information within the confines of the customer organization and for authorized purposes only, subject to compliance with the terms of the license or service agreement with MI Dexigner. Do not share your password with unauthorized parties, and promptly notify MI Dexigner of any unauthorized use. You are responsible for your password's usage and must not attempt unauthorized access to any information or areas on this website.</p>
    <p>We reserve the right to amend our terms and conditions at any time. By continuing to utilize our services post-amendment, you agree to abide by the updated terms and conditions.</p>
    <h2>Email & Mobile Phone Policy</h2>
    <p>This policy delineates the terms and conditions governing the utilization of email and mobile phone contact information provided to MI Dexigner.</p>
    <ol>
        <li>
            <h3>Consent and Usage</h3>
            <p>By supplying your email address and/or mobile phone number, you agree to receive communication from MI Dexigner. This encompasses transactional messages, periodic service or project updates, newsletters, and delivery of services such as design attachments, project updates, and revision notifications.</p>
        </li>
        <li>
            <h3>Carrier Charges Disclosure</h3>
            <p>Standard charges for messaging and data usage may be applicable. Kindly refer to your mobile service provider for further information.</p>
        </li>
        <li>
            <h3>Stop & Unsubscribe</h3>
            <p>To cease receiving emails, simply click the "unsubscribe" link found at the bottom of any email you receive from us. For mobile communications, reply with "<strong>STOP</strong>", "<strong>Unsub</strong>," or "<strong>Unsubscribe</strong>" to any SMS message you receive from us. The SMS service will be promptly terminated.</p>
        </li>
        <li>
            <h3> Frequency of Communication</h3>
            <p>We endeavor to maintain our communication frequency at a reasonable level. Nonetheless, there may be exceptions during special project updates or urgent notifications.</p>
        </li>
        <li>
            <h3>Security of Information</h3>
            <p>Your email address and mobile phone number will be treated as confidential and will not be disclosed to third parties without your explicit consent, except where mandated by law.</p>
        </li>
        <li>
            <h3>Changes to Policy</h3>
            <p>MI Dexigner retains the authority to amend this policy at any time. Any alterations will be published on our website, and it is your responsibility to review these updates.</p>
        </li>
        <li>
            <h3> Contact Us</h3>
            If you have any inquiries or concerns regarding this policy, please reach out to us at <a href="mailto:support@midexigner.com">Email Us Here</a>
        </li>
    </ol>
    </div>
    </section>
    </>);
}
 
export default TermsCondition;