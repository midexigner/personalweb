export default function robots() {
    return {
      rules: {
        userAgent: '*',
        allow: '/',
        // disallow: '/private/',
      },
      sitemap: 'https://midexigner.com/sitemap.xml',
    }
  }