import PageHeader from '../../../components/PageHeader'
import Button from '../../../components/Button';
import Styles from '../../../styles/About.module.css';

const AboutUs = () => {
  return (
    <>
       <PageHeader title={"About Us"} url={"about-us"} />
       <section className="section pt-5">
      <div className="container">
        <div className="layout-grid grid two-column">
          <div className={Styles.aboutContentImage} />
          <div className={Styles.aboutInfo}>
            <h2 className="info-title">Crafting Digital Marvels: A Decade-Long Journey of Web Development Excellence in Pakistan</h2>
            <p>
            I am Muhammad Idrees from Pakistan,I am a seasoned software engineer, amassing over a decade of hands-on experience and specializing in the dynamic realm of WordPress. My proficiency extends across various technologies, including PHP, Django, Shopify, Next.js, and Angular. Throughout my professional journey, I have honed a comprehensive skill set that enables me to navigate the intricate landscape of website development.
            </p>
            <p>
            My expertise goes beyond mere technical proficiency; it is centered around creating user-centric experiences. I pride myself on translating complex concepts into interfaces that not only meet functional requirements but also deliver an intuitive and aesthetically pleasing user experience. This ability to seamlessly blend technical acumen with a keen eye for design has been a cornerstone of my career.</p>
            <div className={Styles.buttonWrapper}>
                <Button url={'/get-a-quote'} text={'Get A Quote'} type="button-color" />
                <Button url={'tel:+923242340583'} text={'Book a Call'} />
              </div>
          </div>
        </div>
          <div className="about-infopadding">
             <div className='layout-grid grid padding center'>
              <div>
                <h3>Empowering Innovation</h3>
                <p>Our mission is to drive transformative solutions, fostering technological evolution. With a commitment to excellence, we aim to redefine possibilities and elevate user experiences in the digital realm.</p>
              </div>
              <div>
              <h3>Envisioning Tomorrow</h3>
              <p>Our vision is to be a pioneering force in shaping the future of technology. We strive to create a world where innovation knows no bounds, where cutting-edge solutions redefine industries, and where our impact resonates globally.</p>
              </div>
             </div>
             </div> 
      </div>
    </section>
    </>
  )
}

export default AboutUs