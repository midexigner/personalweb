'use client';
import Styles from '../../../styles/GetQuote.module.css'
import QueryForm from '../../../components/QueryForm';

const getAQuote = () => {
  const handleGoBack = () => {
    window.history.back();
  };
  return (
    <>
        <section className={`section ${Styles.getAQuote}`}>
            
            <div className={`container ${Styles.containerInner}`}>
            <div className={`layout-grid ${Styles.layoutInner}`}>
            <div className={Styles.leftCols}>
            
                <h1>Your voice matters – we're eager to listen and learn from you!</h1>
                <p>Attention! Signing up for MI Dexigner services and packages is a must. We're here to turn your project dreams into a successful reality.</p>
            <QueryForm />
            </div>
            <div className={Styles.rightCols}>
              <div className={Styles.contactClose} onClick={handleGoBack}></div>
                <p className="Header_logo__2NvDa">MI Dexigner<span className="Header_dot__2Qg1Z">.</span></p>
                
                </div>
            </div>
            </div>
        </section>
    </>
  )
}

export default getAQuote
