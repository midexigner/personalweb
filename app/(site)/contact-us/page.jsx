import PageHeader from '../../../components/PageHeader'
import QueryForm from '../../../components/QueryForm';

const ContactUs = () => {
  return (
    <>
    <PageHeader title={"Contact Us"} url={"contact-us"} />
    <section className='pt-4 pb-4'>
        <div className="container contact-content ">
            <div className="form-block-contact">
              <QueryForm />
            </div>
            <div className="contact-info">
              <h2 className='contact-heading'>Let's Get in Touch</h2>
              <p className='large-paragraph'>Your work is going to fill a large part of your life, and the only way to be truly satisfied is to do what you believe is great work.</p>
            <div className="image-contact"></div>
            </div>
        </div>
    </section>
    </>
  )
}

export default ContactUs