export default function sitemap() {
    return [
      {
        url: 'https://midexigner.com',
        lastModified: new Date(),
        changeFrequency: 'yearly',
        priority: 1,
      },
      {
        url: 'https://midexigner.com/about-us',
        lastModified: new Date(),
        changeFrequency: 'monthly',
        priority: 0.8,
      },
      {
        url: 'https://midexigner.com/services',
        lastModified: new Date(),
        changeFrequency: 'monthly',
        priority: 0.8,
      },
      {
        url: 'https://midexigner.com/portfolio',
        lastModified: new Date(),
        changeFrequency: 'monthly',
        priority: 0.8,
      },
      {
        url: 'https://midexigner.com/get-a-quote',
        lastModified: new Date(),
        changeFrequency: 'monthly',
        priority: 0.8,
      },
      {
        url: 'https://midexigner.com/contact-us',
        lastModified: new Date(),
        changeFrequency: 'monthly',
        priority: 0.8,
      },
      // {
      //   url: 'https://midexigner.com/blog',
      //   lastModified: new Date(),
      //   changeFrequency: 'weekly',
      //   priority: 0.5,
      // },
    ]
  }