'use client'
import Button from '../components/Button'
// import {useEffect} from 'react'
// import {useRouter} from 'next/router'

const NotFound = () => {
  return (
    <>
      <div className="utility-page-wrap">     
         <div className="utility-page-content">
         <h1>Page Not Found</h1>
      <p>The page you are looking for doesn't exist or has been moved</p>
      <Button url="/" text="Go to Homepage" />
         </div>
      </div>
    </>
  );
}
 
export default NotFound