import {defineType} from 'sanity';

export default defineType({
    name: 'homepage',
    title: 'Home page',
    type: 'document',
    // __experimental_actions: [
        // /*'create',*/ 'update', /*'delete',*/ 'publish'],
    fields:[
        {
            title:"Home Page",
            type: 'object',
            name: 'home',
            fieldsets: [
              {name: 'hero', title: 'Hero Section',options: {
                collapsible: true, 
                collapsed: false, 
                columns: 2 
              }},
              {name: 'about', title: 'About Section',options: {
                collapsible: true, 
                collapsed: true, 
                columns: 1 
              }},
              {name: 'ourservice', title: 'Service Section',options: {
                collapsible: true, 
                collapsed: true, 
                columns: 1 
              }},
              {name: 'recentwork', title: 'Portfolio Section',options: {
                collapsible: true, 
                collapsed: true, 
                columns: 1 
              }}
            ],
            fields: [
             
              {
                name: 'title',
                title: 'Title',
                type: 'string',
                fieldset: 'hero'
              },
              {
                name: 'subtitle',
                title: 'Sub Title',
                type: 'string',
                fieldset: 'hero'
              },
              {
                name: 'description',
                title: 'Description',
                type: 'array', 
                of: [{type: 'block'}],
                fieldset: 'hero'
              },
              {
                title: 'Button One URL',
                name: 'btnOneURL',
                type: 'url',
                validation: Rule => Rule.uri({
                  scheme: ['http', 'https', 'mailto', 'tel']
                }),
                fieldset: 'hero'
              },
              {
                title: 'Button One Text',
                name: 'btnOneText',
                type: 'string',
                fieldset: 'hero'
              },
              {
                title: 'Button Two URL',
                name: 'btnTwoURL',
                type: 'url',
                validation: Rule => Rule.uri({
                  scheme: ['http', 'https', 'mailto', 'tel']
                }),
                fieldset: 'hero'
              },
              {
                title: 'Button Two Text',
                name: 'btnTwoText',
                type: 'string',
                fieldset: 'hero'
              },
              {
                title: 'Banner',
                name: 'banner',
                type: 'image',
                options: {
                  hotspot: false,
              },
                fieldset: 'hero'
              },    
              //service
              {
                name: 'ourservicetitle',
                title: 'Title',
                type: 'string',
                fieldset: 'ourservice'
              },
             
              {
                title: 'About Banner',
                name: 'homepage_about_aboutBanner',
                type: 'image',
                fieldset: 'about'
              },
              {
                name: 'homepage_about_title',
                title: 'About Title',
                type: 'string',
                fieldset: 'about'
              },
              {
                name: 'homepage_about_content',
                title: 'About Content',
                type: 'string',
                fieldset: 'about'
              },
              {
                title: 'About Button URL',
                name: 'homepage_button_url',
                type: 'url',
                validation: Rule => Rule.uri({
                  scheme: ['http', 'https', 'mailto', 'tel']
                }),
                fieldset: 'about'
              },
              {
                name: 'homepage_button_text',
                title: 'About Button Text',
                type: 'string',
                fieldset: 'about'
              },
              {
                name: 'homepage_portfolio_title',
                title: 'Portfolio Title',
                type: 'string',
                fieldset: 'recentwork'
              },
              {
                title: 'Button URL',
                name: 'homepage_portfolio_btn_url',
                type: 'url',
                validation: Rule => Rule.uri({
                  scheme: ['http', 'https', 'mailto', 'tel']
                }),
                fieldset: 'recentwork'
              },
              {
                name: 'homepage_portfolio_btn_text',
                title: 'Button Text',
                type: 'string',
                fieldset: 'recentwork'
              },
              
            ]
          }
    ],
    preview: {
      select: {
        title: 'hero.title',
      },
      
    },
});