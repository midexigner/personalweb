export const structure = (S, context) => 
  S.list()
    .title('Site Content')
    .items([
      S.listItem()
        .title('Site Settings')
        .child(
          S.editor()
            .id('globalSettings')
            .schemaType('globalSettings')
            .documentId('globalSettings')
        ),
        S.listItem()
        .title('Home Page')
        .child(
          S.editor()
            .id('homepage')
            .schemaType('homepage')
            .documentId('homepage')
        ),
        ...S.documentTypeListItems().filter(
          (listItem) =>
            ![
              'globalSettings',
              'homepage',
              // 'category',
              // 'author',
            ].includes(listItem.getId())
        ),
      ])
       
    