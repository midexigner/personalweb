```sql
CREATE TABLE orders (
    id UUID primary KEY,
    client_id UUID NOT NULL,
    email TEXT,
    name TEXT,
    address TEXT,
    zip_code TEXT,
    city TEXT,
    created_at TIMESTAMP NOT NULL DEFAULT NOW()
);
ALTER TABLE Orders ENABLE ROW LEVEL SECURITY;

CREATE TABLE clients (
    id UUID NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    email TEXT,
    created_at TIMESTAMP NOT NULL DEFAULT NOW()
);

ALTER TABLE clients ENABLE ROW LEVEL SECURITY;

```

### Trigger

```sql
CREATE TABLE public.users (
    id uuid REFERENCES auth.users NOT NULL PRIMARY KEY,
    email text
);

CREATE OR REPLACE FUNCTION public.handle_new_user()
RETURNS TRIGGER AS $$
BEGIN
    INSERT INTO public.users (id, email)
    VALUES (NEW.id, NEW.email);
    RETURN NEW;
END;
$$ LANGUAGE plpgsql SECURITY DEFINER;

CREATE TRIGGER on_new_user
AFTER INSERT ON auth.users
FOR EACH ROW EXECUTE PROCEDURE public.handle_new_user();



```

### Schema

```sql
CREATE schema sales;

CREATE TABLE sales.customers(
    id uuid not null primary key,
    name TEXT,
    email TEXT
);
```

### Storage
```sql
-- Enable row-level security
ALTER TABLE storage.objects ENABLE ROW LEVEL SECURITY;

-- Policy for SELECT operations
CREATE POLICY object_select_policy ON storage.objects FOR SELECT USING (auth.role()='authenticated');

-- Policy for INSERT operations WITH CHECK !
CREATE POLICY object_insert_policy ON storage.objects FOR INSERT WITH CHECK (auth.role()='authenticated');

-- Policy for UPDATE operations
CREATE POLICY object_update_policy ON storage.objects FOR UPDATE USING (auth.role()='authenticated');

-- Policy for DELETE operations
CREATE POLICY object_delete_policy ON storage.objects FOR DELETE USING (auth.role()='authenticated');

```